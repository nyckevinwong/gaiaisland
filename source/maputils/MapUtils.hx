package maputils;

import flixel.tile.FlxTilemap;
import flixel.FlxSprite;
import flixel.tile.FlxBaseTilemap.FlxTilemapAutoTiling;
import maputils.tile.AnimatedTiledMap;
import maputils.tile.FlxAnimatedTilemapExt;

import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledLayer.TiledLayerType;
import flixel.addons.tile.FlxTilemapExt;

import flixel.FlxObject;
/**
 * ...
 * @author Kevin Wong
 */
class MapUtils 
{

	/*
	public static function getMiniMap(?wallColor:Int = 0xFF000000, ?openColor:Int = 0xFFFFFFFF):FlxSprite
	{
	//Create Minimap
	var minimap:FlxSprite = new FlxSprite(FlxG.width / 2 - Reg.level.widthInTiles / 2, FlxG.height / 2 + 16 );
	minimap.makeGraphic(Reg.level.widthInTiles, Reg.level.heightInTiles, 0xFFFF0000);
	
	//Set bitmap data
	var bData:BitmapData = new BitmapData(Reg.level.widthInTiles, Reg.level.heightInTiles);
	for (i in 0...Reg.level.totalTiles) {
		if (!Reg.level.overlapsPoint(FlxPoint.get(i % Reg.level.widthInTiles * 16, i / Reg.level.widthInTiles * 16))) bData.setPixel32(i % Reg.level.widthInTiles, Math.floor(i / Reg.level.widthInTiles), openColor);
		else bData.setPixel32(i % Reg.level.widthInTiles, Math.floor(i / Reg.level.widthInTiles), wallColor);
	}
	
	//Set Minimap bitmap data
	minimap.pixels = bData;
	return minimap;
	}*/
	
	public static function LoadFlxTilemap(data:FlxTiledMapAsset, ?AutoTile:FlxTilemapAutoTiling, StartingIndex:Int = 0, DrawIndex:Int = 1, CollideIndex:Int = 1 ): FlxTilemap
	{
		 var _mapLoader:AnimatedTiledMap= new AnimatedTiledMap(data);
		 var _map:FlxTilemapExt = new FlxTilemapExt();
		 var tileLayer: TiledTileLayer = cast(_mapLoader.getLayer("walls"), TiledTileLayer);
		 _map.loadMapFromArray(tileLayer.tileArray, tileLayer.width, tileLayer.height, AssetPaths.tiles__png, _mapLoader.tileWidth, _mapLoader.tileHeight, AutoTile, StartingIndex, DrawIndex, CollideIndex);
		 _mapLoader.loadAnimationForTiledTileLayer(_map, tileLayer);
		 return _map;
	}
	
	public static function overlapWhichTiles(x:Float, y:Float, width:Float, height:Float, map : FlxAnimatedTilemapExt ): Array<Int>
	{
		// 16 means tile size
		var tileX :Int = Std.int(x  / map.tileWidth);   
		var tileX1: Int = Std.int(x + width / map.tileWidth);
		var tileY :Int  = Std.int(y/  map.tileHeight);   
		var tileY1 :Int  = Std.int((y + height - 1) /  map.tileHeight);   
		var array: Array<Int> = new Array<Int>();

	
		array.push( getTileByCoord(map, tileX, tileY) ); // upper-left
		array.push( getTileByCoord(map,tileX1, tileY) ); // upper-right
		array.push( getTileByCoord(map,tileX, tileY1) ); // lower-left
		array.push( getTileByCoord(map,tileX1, tileY1) ); // lower-right

		return array;
	}

	public static function horizontalRayOverlapTiles(x:Float, y:Float, width:Float, map : FlxAnimatedTilemapExt): Array<Int>
	{
		// 16 means tile size
		var tileX :Int = Std.int(x  / map.tileWidth);   
		var tileX1: Int = Std.int(x + width / map.tileWidth);
		var tileY :Int  = Std.int(y/  map.tileHeight);   
		var array: Array<Int> = new Array<Int>();


		var rayLength: Int = Std.int(width / map.tileWidth);
		
		for (i in 0...rayLength)
		{
			var tileX :Int = Std.int(x  / map.tileWidth) + i;   
			array.push( getTileByCoord(map, tileX, tileY) );
		}

		return array;
	}

	public static function horizontalRayOverlapAnyTile(x:Float, y:Float, width:Float, map : FlxAnimatedTilemapExt): Bool
	{
		// 16 means tile size
		var tileX :Int = Std.int(x  / map.tileWidth);   
		var tileX1: Int = Std.int(x + width / map.tileWidth);
		var tileY :Int  = Std.int(y/  map.tileHeight);   


		var rayLength: Int = Std.int(width / map.tileWidth);
		
		for (i in 0...rayLength)
		{
			var tileX :Int = Std.int(x  / map.tileWidth) + i;   
			var tileIndex: Int =  getTileByCoord(map, tileX, tileY);
			
			if (tileIndex != 0)
				return true; // some tiles are on this ray
		}

		return false;
	}
	
	public static function getTileByCoord(map : FlxAnimatedTilemapExt, x:Int, y: Int) : Int
	{
				// check the next tile on the right side;	
				if (x < 0 || x > map.widthInTiles-1)
				{
					return FlxObject.NONE;
				}
				
				if (y < 0 || y > map.heightInTiles-1)
				{
					return FlxObject.NONE;
				}
				
				var tileId = map.getTile(x, y);
				return tileId;
	}
	
	public static function standOnWhichTiles(map : FlxAnimatedTilemapExt, x:Float, y: Float, charHeight: Float): Array<Int>
	{
		// 16 means tile size
		var tileX :Int = Std.int(x  /  map.tileWidth);   
		var tileY :Int  = Std.int((y + charHeight + 1) /  map.tileHeight);   
		var array: Array<Int> = new Array<Int>();
			
		array.push( getTileByCoord(map, tileX, tileY) );
		array.push( getTileByCoord(map, tileX + 1, tileY) );
		
		return array;
	}
	
}