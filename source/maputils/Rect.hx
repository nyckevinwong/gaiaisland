package maputils;

import flixel.math.FlxRect;


/**
 * ...
 * @author ...
 */
class Rect 
{
	public var x:Float;
	public var y:Float;
	public var width:Float;
	public var height:Float;
	public var name:String;
	
	/**
	 * The x coordinate of the left side of the rectangle.
	 */
	public var left(get, set):Float;
	
	/**
	 * The x coordinate of the right side of the rectangle.
	 */
	public var right(get, set):Float;
	
	/**
	 * The x coordinate of the top of the rectangle.
	 */
	public var top(get, set):Float;
	
	/**
	 * The y coordinate of the bottom of the rectangle.
	 */
	public var bottom(get, set):Float;
	

	public function new(X:Float = 0, Y:Float = 0, Width:Float = 0, Height:Float = 0)
	{
		set(X, Y, Width, Height);
	}
	
	private inline function get_left():Float
	{
		return x;
	}
	
	private inline function set_left(Value:Float):Float
	{
		width -= Value - x;
		return x = Value;
	}
	
	private inline function get_right():Float
	{
		return x + width;
	}
	
	private inline function set_right(Value:Float):Float
	{
		width = Value - x;
		return Value;
	}
	
	private inline function get_top():Float
	{
		return y;
	}
	
	private inline function set_top(Value:Float):Float
	{
		height -= Value - y;
		return y = Value;
	}
	
	private inline function get_bottom():Float
	{
		return y + height;
	}
	
	private inline function set_bottom(Value:Float):Float
	{
		height = Value - y;
		return Value;
	}
	
	private inline function get_isEmpty():Bool
	{
		return width == 0 || height == 0;
	}
	
	/**
	 * Returns true if this FlxRect contains the FlxPoint
	 * 
	 * @param	Point	The FlxPoint to check
	 * @return	True if the FlxPoint is within this FlxRect, otherwise false
	 */
	public function containsPoint(pointX:Float, pointY:Float):Bool
	{
		var result:Bool = pointX >= this.x && pointX <= this.right && pointY >= this.y && pointY <= this.bottom;
		return result;
	}
	
	public inline function overlaps(rect:Rect):Bool
	{
		var result =
			(rect.x + rect.width > x) &&
			(rect.x < x + width) &&
			(rect.y + rect.height > y) &&
			(rect.y < y + height);
			
		return result;
	}
	
	public inline function overlapsByFlxRect(rect:FlxRect):Bool
	{
		var result =
			(rect.x + rect.width > x) &&
			(rect.x < x + width) &&
			(rect.y + rect.height > y) &&
			(rect.y < y + height);
			
		return result;
	}
	/**
	 * Fill this rectangle with the data provided.
	 * 
	 * @param	X		The X-coordinate of the point in space.
	 * @param	Y		The Y-coordinate of the point in space.
	 * @param	Width	Desired width of the rectangle.
	 * @param	Height	Desired height of the rectangle.
	 * @return	A reference to itself.
	 */
	public inline function set(X:Float = 0, Y:Float = 0, Width:Float = 0, Height:Float = 0)
	{
		x = X;
		y = Y;
		width = Width;
		height = Height;
	}


}