package maputils;

import haxe.ds.StringMap;
import utils.GameSave;
import flixel.util.FlxSave;
/**
 * ...
 * @author ...
 */
class Levels 
{
	private static var levels : Array<String> = new Array<String>();  
	private static var leveltitles : Array<String> = new Array<String>();  
	

	private static var curLevel: Int = 0;
	private static var backToFirstLevel = true;
	private static var maxTestLevel: Int = 0;
	
	public static function init()
	{
		if (levels.length == 0)
		{
			#if (windows)
			Levels.push("test-1", AssetPaths.test__tmx);
			Levels.push("test-2", AssetPaths.test__tmx);
			Levels.push("test-3", AssetPaths.test__tmx);
			#end			
			Levels.push("1-1", AssetPaths.level1__tmx);
			Levels.push("1-2", AssetPaths.level2__tmx);
			Levels.push("1-3", AssetPaths.level3__tmx);			
			
			var testLevels: Array<String> = 
			Levels.levels.filter(function(name:String):Bool
			{
				return (name.indexOf("test") >= 0);
			});
			
			maxTestLevel = testLevels.length;
			
		}
		
	}
	
	public static function push(title:String, level:String)
	{
		levels.push(level);
		leveltitles.push(title);
	}
		
	public function new() 
	{
		Levels.init();
	}
	
	public static function setLevel(level:Int)
	{
		curLevel = level;

		if(curLevel>= levels.length || curLevel < 0)
		{
			curLevel = 0; // back to level zero
		}
	}
	
	public static function totalLength(): Int
	{
		return Levels.levels.length;
	}
	
	public static function getMaxTestLevel() :Int
	{
		return Levels.maxTestLevel;
	}

	
	public static function unlockLevel(index:Int):Bool
	{
		if (index < 0 || index >= Levels.levels.length)
		return false;
		
		var currentSave: FlxSave = GameSave.read();
		
		if (currentSave.data.unlockedLevels != null)
		{
			currentSave.data.unlockedLevels[index] = true;
			currentSave.flush(); // required for non-flash target
			return true;
		}
		
		return false;
	}
	
	public static function isLevelUnlocked(index:Int):Bool
	{
		if (index < 0 || index >= Levels.levels.length)
		return false;
		
		var currentSave: FlxSave = GameSave.read();
		
		if (currentSave.data.unlockedLevels != null)
		{
			return currentSave.data.unlockedLevels[index];
		}
		
		return false; // consider it's locked if the game record is not available!!
	}
	
	public static function next():Bool
	{
		if (curLevel < levels.length)
		{
			curLevel++;

			if(curLevel>= levels.length && backToFirstLevel)
			{
				curLevel = 0; // back to level zero
			}
			else
			{
				if (curLevel > Levels.maxTestLevel)
				{
					unlockLevel(curLevel - Levels.maxTestLevel); // try to unlock the next level
				}
			}
			
			return true;
		}
		
		
		return false;
	}
	
	public static function current(): String
	{
		return levels[curLevel];
	}
	
	public static function currentTitle() : String
	{
		return leveltitles[curLevel];
	}
	
	public static function getTitle(index:Int) : String
	{
		if (index <0 || index >= Levels.leveltitles.length)
		{
			return null;
		}
		
		return leveltitles[index];
	}
	
	public function isNoMoreLevel(): Bool
	{
		return curLevel >= levels.length;
	}
	
	
}