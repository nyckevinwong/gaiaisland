package maputils;


import flixel.util.FlxDestroyUtil.IFlxDestroyable;

/**
 * ...
 * @author ...
 */
class Areas implements IFlxDestroyable
{
	private var areaMaps:Map<String, Rect> = new Map<String, Rect>();
	
	public function new()
	{
	
	}
	
	public function add(name: String, rect: Rect)
	{
			areaMaps.remove(name);
			areaMaps.set(name, rect);
	}
	
	public function get(name:String): Rect
	{
		return areaMaps.get(name);
	}
	
	public function getName(targetRect:Rect): String
	{
		for (name in areaMaps.keys()) {
			var rect: Rect = areaMaps.get(name);
			if (targetRect == rect)
			return name;
		}
		
		return null;
	}

	public function getInfo(): String
	{
		var s:String = "";
		
		for (name in areaMaps.keys()) {
			var r: Rect = areaMaps.get(name);
			s += name + ": (" + r.x + "," + r.y + "," + r.width  + "," +  r.height +")";
			s += "\n";
		}
		
		return s;
	}
	
	public function containsPoint(X: Float, Y: Float): Rect
	{
		for (name in areaMaps.keys()) {
			var rect: Rect = areaMaps.get(name);
			if (rect.containsPoint(X, Y))
			return rect;
		}
		
		return null; // not found
	}
	
	public function destroy():Void
	{
		for (name in areaMaps.keys()) {
			var rect: Rect = areaMaps.get(name);
			areaMaps.remove(name);
			rect = null;
		}

	}
			
}