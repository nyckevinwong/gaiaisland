package maputils.tile;

import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledTile;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.addons.tile.FlxTilemapExt;
import flixel.addons.tile.FlxTileSpecial;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.math.FlxRect;
import flixel.util.FlxSort;
import flixel.group.FlxGroup;
import maputils.tile.TileAnims.AnimData;
/**
 * ...
 * @author ...
 */
class AnimatedTiledMap extends TiledMap
{
	var animations: Map<Int, Array<AnimData>> = null;
	var animationFile:FlxTiledMapAsset = null;
	
	public function new(data:FlxTiledMapAsset, rootPath:String="")
	{
		super(data, rootPath);
		animationFile = data;
		animations = loadAnimations(true);
	}
	
	public function loadAnimations(reload:Bool = false)
	{
		if (animations == null || reload)
		{
			animations = TileAnims.getAnimations(animationFile);
		}
		
		return animations;
	}

	private inline function isSpecialTile(tile:TiledTile, animations:Dynamic):Bool
	{
		return tile.isFlipHorizontally || tile.isFlipVertically || tile.rotate != FlxTileSpecial.ROTATE_0 || animations.exists(tile.tilesetID);
	}
	
	public function loadAnimationForTiledTileLayer(map:FlxTilemapExt, layer:TiledTileLayer)
	{

		/*
		20 frames as an example 
		1000 / total frame Count 
		1000/20 = 50 
		50 millon second per frame

		(1)  N ms duration per frame = 1000 / total frame Count
		(2)  desired Duration / N = desired multiplier
		(3) total Frame Count / desired multiplier => correct frame rate for the desired duration
		*/
 
		var specialTiles:Array<FlxTileSpecial> = new Array<FlxTileSpecial>();
		var tile:TiledTile;
		var specialTile:FlxTileSpecial;
		var animData: AnimData;
		
			for (i in 0...layer.tiles.length)
			{ 
				tile = layer.tiles[i];
				if (tile != null && isSpecialTile(tile, animations))
				{
					specialTile = new FlxTileSpecial(tile.tilesetID, tile.isFlipHorizontally, tile.isFlipVertically, tile.rotate);
					// add animations if exists
					if (animations.exists(tile.tilesetID))
					{
						// Right now, a special tile only can have one animation.
						animData = animations.get(tile.tilesetID)[0];
						// add some speed randomization to the animation
					
						// NEVER USE RANDOMIZED frame speed/duration
						//	var randomize:Float = FlxG.random.float( -animData.randomizeSpeed, animData.randomizeSpeed);
					//	var speed:Float = animData.speed + randomize;
					
						// only support const duration over all frames.
						var durationPerFrame: Float = 1000 / animData.frames.length;
						var desiredMultiplier :Float = animData.speed / durationPerFrame;
						var correctFrameRate : Float = animData.frames.length / desiredMultiplier;
						
						
						specialTile.addAnimation(animData.frames, correctFrameRate, animData.framesData);
					}

					
					specialTiles[i]= specialTile;
				}
				else
				{
					specialTiles[i] = null;
				}
		
			}
			// set the special tiles (flipped, rotated and/or animated tiles)
			map.setSpecialTiles(specialTiles);

	}
	
}