package maputils.tile;

import flixel.addons.tile.FlxTilemapExt;
/**
 * ...
 * @author Kevin Wong
 */
class FlxAnimatedTilemapExt extends FlxTilemapExt
{
	public var tileWidth:Int = 16;
	public var tileHeight:Int = 16;

	public function removeSpecialTile(tilePosIndex:Int)
	{
		this._specialTiles[tilePosIndex] = null;
	}
	
	public function getTileArrayIndex(tileX:Int, tileY:Int):Int	
	{
		return tileY * this.widthInTiles + tileX;
	}
	
	public override function setTile(X:Int, Y:Int, Tile:Int, UpdateGraphics:Bool = true):Bool
	{
		var tileArrayIndex: Int = this.getTileArrayIndex(X, Y);
		this.removeSpecialTile(tileArrayIndex); // stop animation by making it not speical anymore in case it's a speical animated tile
		
		return super.setTile(X, Y, Tile, UpdateGraphics);
	}
}