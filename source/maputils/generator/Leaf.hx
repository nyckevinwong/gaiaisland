package maputils.generator;

import flash.geom.Rectangle;
import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.math.FlxRandom;

class Leaf
{

	public static inline var MIN_WIDTH : Int = 20;
	public static inline var MIN_HEIGHT : Int= 13;
	public static inline var MAX_WIDTH : Int= 180;
	public static inline var MAX_HEIGHT : Int = 32;
	public static var RANDOM: FlxRandom = new FlxRandom(1); // use fixed seed to generate the same map on the same level
	public var level:Int = 0;
	
	public var x:Int;
	public var y:Int;
	public var width:Int;
	public var height:Int;

	public var leftChild:Leaf;
	public var rightChild:Leaf;
	public var room:Rectangle;
	public var hallways:Array<Rectangle>;
	
	public function new(x:Int, y:Int, width:Int, height:Int, level: Int)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.level = level;
	}

	public function split():Bool
	{
		// Split leaf into 2 children
		if (leftChild != null || rightChild != null)
			return false; // Already split

		// Determine split direction
		// If width >25% larger than height, split vertically
		// Else if height >25% larger than width, split horizontally
		// Else split randomly
		var splitV:Bool = false;
		var ToSplitThisLeaf: Bool = false;
		
		if (level <= 1)
		{
			splitV = RANDOM.float() > 0.5; // 50% chance to split vertically
			ToSplitThisLeaf = true; // always split under 2 level
		}
		else if (level <= 2 )
		{
			splitV = RANDOM.float() > 0.3; // 70% chance to split vertically
			ToSplitThisLeaf = RANDOM.float() > 0.5; // 50% chance to split under level 3
		}
		else if (level >= 4)
		{
			return false; // no more split on level 4
		}
		else
		{
			splitV = RANDOM.float() > 0.1; // 90% chance to split horizontally
			ToSplitThisLeaf = RANDOM.float() > 0.5; // 50% chance to split under level 3
		}
		
		if (!ToSplitThisLeaf)
		return false;
		
		var curMaxWidth : Int = width-MIN_WIDTH;
		var curMaxHeight: Int = height-MIN_HEIGHT;
		
		if (splitV)
		{
			if(curMaxHeight < MIN_HEIGHT)
			{
				if (curMaxWidth < MIN_WIDTH)
				{
					return false; // unable to splite at all
				}
				else
				{
					splitV = false; // but you can still split it horizontally
				}
			}
		}
		else
		{
			if(curMaxWidth < MIN_WIDTH)
			{
				if (curMaxHeight < MIN_HEIGHT)
				{
					return false; // unable to splite at all
				}
				else
				{
					splitV = true; // but you can still split it vertically 
				}
			}
		}
		
		curMaxHeight = Std.int(Math.min(curMaxHeight, MAX_HEIGHT)); // use max height if curMaxHeight is larger
		curMaxWidth = Std.int(Math.min(curMaxWidth, MAX_WIDTH)); // use max height if curMaxWidth is larger
		
		
		var splitHeight: Int = Std.int(RANDOM.int(MIN_HEIGHT, curMaxHeight));			
		var splitWidth: Int = Std.int(RANDOM.int(MIN_WIDTH, curMaxWidth));			
		
		if (level <= 2)
		{
			// 50% of chance to use max width/ height
			if ( RANDOM.float() > 0.5 )
			{
				splitWidth = curMaxWidth;
				splitHeight = curMaxHeight;
			}
		}
		
		if (splitV)
		{
			leftChild = new Leaf(x, y, width, splitHeight, level+1);
			rightChild = new Leaf(x, y + splitHeight, width, height - splitHeight, level+1);
		}
		else
		{
			leftChild = new Leaf(x, y, splitWidth, height, level+1);
			rightChild = new Leaf(x + splitWidth, y, width - splitWidth, height, level+1);
		}
		
		/*
		  
		if (width > height && height / width >= 0.05)
			splitH = false;
		else if (height > width && width / height >= 0.05)
			splitH = true;
			
	

		var max = (splitV ? height : width) - MIN_SIZE; // determine the maximum height or width
		if (max <= MIN_SIZE)
			return false; // the area is too small to split any more...

		// Where to split
		var split = Std.int(RANDOM.float(MIN_SIZE, max)); // determine where we're going to split
		

		// Create children based on split direction
		if (splitV)
		{
			leftChild = new Leaf(x, y, width, split);
			rightChild = new Leaf(x, y + split, width, height - split);
		}
		else
		{
			leftChild = new Leaf(x, y, split, height);
			rightChild = new Leaf(x + split, y, width - split, height);
		}*/

		return true;
	}

	public function getRoom():Rectangle
	{
		if (room != null)
			return room;
		else
		{
			var lRoom:Rectangle = null;
			var rRoom:Rectangle = null;
			if (leftChild != null)
			{
				lRoom = leftChild.getRoom();
			}
			if (rightChild != null)
			{
				rRoom = rightChild.getRoom();
			}
			if (lRoom == null && rRoom == null)
				return null;
			else if (rRoom == null)
				return lRoom;
			else if (lRoom == null)
				return rRoom;
			else if (RANDOM.float() > .5)
				return lRoom;
			else
				return rRoom;
		}
	}

	public function createRooms():Void
	{
		// Generates all rooms and hallways for this leaf and its children
		if (leftChild != null || rightChild != null)
		{
			// This leaf has been split, go to children leafs
			if (leftChild != null)
			{
				leftChild.createRooms();
			}
			if (rightChild != null)
			{
				rightChild.createRooms();
			}

			/* NO NEED A HALL
			 * 
			// If both left/right children in leaf, make hallway between them
			if (leftChild != null && rightChild != null)
			{
				createHall(leftChild.getRoom(), rightChild.getRoom());
			}*/
		}
		else
		{
			/*
			// Room can be between 3x3 tiles to the leaf size - 2
			var roomSize = new FlxPoint(
				RANDOM.float(3, width - 2),
				RANDOM.float(3, height - 2));
			// Place the room within leaf, but not against sides (would merge)
			var roomPos = new FlxPoint(
				RANDOM.float(1, width - roomSize.x - 1),
				RANDOM.float(1, height - roomSize.y - 1));
				
				
			room = new Rectangle(x + roomPos.x, y + roomPos.y, roomSize.x, roomSize.y);
			*/
			room = new Rectangle(x+1, y+1, width-2, height-2);
			
		}
	}

	public function createHall(left:Rectangle, right:Rectangle):Void
	{
		// Connects 2 rooms together with hallways
		hallways = [];

		var point1 = FlxPoint.get(
			RANDOM.float(left.left + 1, left.right - 2),
			RANDOM.float(left.top + 1, left.bottom - 2));
		var point2 = FlxPoint.get(
			RANDOM.float(right.left + 1, right.right - 2),
			RANDOM.float(right.top + 1, right.bottom - 2));

		var w = point2.x - point1.x;
		var h = point2.y - point1.y;

		if (w < 0)
		{
			if (h < 0)
			{
				if (RANDOM.float() > 0.5)
				{
					hallways.push(new Rectangle(point2.x, point1.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point2.x, point2.y, 1, Math.abs(h)));
				}
				else
				{
					hallways.push(new Rectangle(point2.x, point2.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point1.x, point2.y, 1, Math.abs(h)));
				}
			}
			else if (h > 0)
			{
				if (RANDOM.float() > 0.5)
				{
					hallways.push(new Rectangle(point2.x, point1.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point2.x, point1.y, 1, Math.abs(h)));
				}
				else
				{
					hallways.push(new Rectangle(point2.x, point2.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point1.x, point1.y, 1, Math.abs(h)));
				}
			}
			else
			{
				hallways.push(new Rectangle(point2.x, point2.y, Math.abs(w), 1));
			}
		}
		else if (w > 0)
		{
			if (h < 0)
			{
				if (RANDOM.float() > 0.5)
				{
					hallways.push(new Rectangle(point1.x, point2.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point1.x, point2.y, 1, Math.abs(h)));
				}
				else
				{
					hallways.push(new Rectangle(point1.x, point1.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point2.x, point2.y, 1, Math.abs(h)));
				}
			}
			else if (h > 0)
			{
				if (RANDOM.float() > 0.5)
				{
					hallways.push(new Rectangle(point1.x, point1.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point2.x, point1.y, 1, Math.abs(h)));
				}
				else
				{
					hallways.push(new Rectangle(point1.x, point2.y, Math.abs(w), 1));
					hallways.push(new Rectangle(point1.x, point1.y, 1, Math.abs(h)));
				}
			}
			else
			{
				hallways.push(new Rectangle(point1.x, point1.y, Math.abs(w), 1));
			}
		}
		else
		{
			if (h < 0)
			{
				hallways.push(new Rectangle(point2.x, point2.y, 1, Math.abs(h)));
			}
			else if (h > 0)
			{
				hallways.push(new Rectangle(point1.x, point1.y, 1, Math.abs(h)));
			}
		}
	}
}