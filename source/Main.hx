package;

import flixel.FlxGame;
import openfl.display.Sprite;
import flash.events.Event;
import screenstates.TitleState;
import utils.LogoState;
import utils.AdMobManager;
import maputils.generator.GenerateMapState;
import flixel.FlxG;
import flixel.addons.ui.FlxUIState;
import utils.FireTongueEx;
import openfl.text.Font;
import openfl.Assets;
import screenstates.PlayState;

class Main extends Sprite
{
	public static var SCREEN_WIDTH:Int = 320;//432;
	public static var SCREEN_HEIGHT:Int = 208; // 240;
	public static var tongue:FireTongueEx;
	
	public function new()
	{
		super(); 
		
		
		haxe.Log.trace = function(v:Dynamic, ?infos:haxe.PosInfos) { 
		//custom trace function here
		}

		if (Main.tongue == null)
		{
			Main.tongue = new FireTongueEx();
			Main.tongue.init("en-US");
			FlxUIState.static_tongue = Main.tongue;
		}
		
		LogoState.init(TitleState); // specify next state after displaying the company logo
		
		var game:FlxGame = new FlxGame(SCREEN_WIDTH, SCREEN_HEIGHT, LogoState , 1 , 60, 60, true);

		AdMobManager.init();
		

	
		addChild(game);
	}
	
}