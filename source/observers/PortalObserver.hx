package observers;

import designpatterns.observer.Observable;
import designpatterns.observer.Observer;
import chars.Character;
import maputils.Rect;
import maputils.Areas;
import flixel.FlxG;
import chars.GameObject;

/**
 * ...
 * @author ...
 */
class PortalObserver implements Observer<Character> 
{
	private var areas: Areas;
	
	public function new(areas: Areas) 
	{
		this.areas = areas;
	}
	
	public function updateArea(areas: Areas) 
	{
		this.areas = areas;
	}
	
	public function update(observable:Observable<Character>, c:Character) {
		
		var rect: Rect = this.areas.containsPoint(c.x, c.y);

		c.tempInfo.scrollFactor.set(0, 0);
		c.tempInfo.x = 200;
		c.tempInfo.y = 200;
		c.tempInfo.textField.scaleX = 2;
		c.tempInfo.textField.scaleY = 2;
		
		if (rect != null)
		{
			FlxG.camera.setScrollBoundsRect(rect.left , rect.top, rect.width, rect.height, false);
			GameObject.activeArea = rect;
			c.tempInfo.text = GameObject.activeArea.name;
		}
		else
		{
			c.tempInfo.text = "AREA NOT FOUND!!";
		}
			c.tempInfo.displayInfo();
		//
	}
	
}