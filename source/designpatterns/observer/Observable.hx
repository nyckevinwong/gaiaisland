package designpatterns.observer;

// based on https://github.com/luizbills/haxe-observer/blob/master/src/observer/Observable.hx

//@:generic //  this can yield a boost in performance-critical code portions on static targets at the cost of a larger output size:
class Observable<T> 
{

 private var changed:Bool;
 private var observers:List<Observer<T>>;

  public function new() {
    clearChanged();
    observers = new List<Observer<T>>();
  }

  /**
   * Add an observer to the list of observers
   */
  public function addObserver(obs:Observer<T>):Bool {
    if (obs == null) {
      return false;
    }

    // Don't add an observer more than once
    for (o in observers) {
      if (o == obs) return false;
    }

    // Add the observer in the list
    observers.add(obs);

    return true;
  }

  /**
   * Remove an observer from the list of observers
   */
  public function removeObserver(obs:Observer<T>):Bool {
    return observers.remove(obs);
  }

  /**
   * Remove all observers from the list of observers
   */
  public function removeObservers():Void {
    return observers.clear();
  }

  /**
   * return the number of observers on the list of observers.
   */
  public function countObservers():Int {
    return observers.length;
  }

  /**
   * Tell all observers that the subject has changed
   */
  public function notifyObservers(?data:T):Void {
    if (!changed) {
      return;
    }

    clearChanged();

    for (obs in observers) {
      obs.update(this, data);
    }
  }

  public function setChanged() {
    changed = true;
  }

  public function clearChanged() {
    changed = false;
  }

  public function hasChanged() {
    return changed;
  }
	
}