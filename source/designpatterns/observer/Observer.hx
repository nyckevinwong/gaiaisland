package designpatterns.observer;

// based on https://github.com/luizbills/haxe-observer/blob/master/src/observer/Observable.hx

interface Observer<T> {

  public function update(observable:Observable<T>, data:T):Void;

}