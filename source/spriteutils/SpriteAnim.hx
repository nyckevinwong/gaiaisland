package spriteutils;


import flixel.FlxSprite;
import flixel.math.FlxRect;

/**
 * ...
 * @author Kevin Wong
 */
class SpriteAnim
{
	public static inline var UPAPPEAR:Int = 1;
	public static inline var DOWNAPPEAR:Int = 2;
	public static inline var LEFTAPPEAR:Int = 3;
	public static inline var RIGHTAPPEAR:Int = 4;
	public static inline var UPDISAPPEAR:Int = 5;
	public static inline var DOWNDISAPPEAR:Int = 6;
	public static inline var LEFTDISAPPEAR:Int = 7;
	public static inline var RIGHTDISAPPEAR:Int = 8;
	
	private var value:Float = 0;
	private var targetIncreaseHeight:Float = 0;
	private var targetSprite: FlxSprite;
	private var tempRect: FlxRect;
	private var originalClipRect:FlxRect;
	private var repeat : Bool = true;
	private var finished: Bool = false;
	private var originalY: Float = 0;
	private var targetHeight: Float = 0;
	private var started:Bool = false;
	private var defaultDuration:Float = 1;


	public var scanDirection: Int = SpriteAnim.DOWNAPPEAR;
	
	public function new(sprite:FlxSprite, duration:Float = 1, repeatAnimation:Bool = true) 
	{
		this.targetSprite = sprite;
		this.originalClipRect = this.targetSprite.clipRect;
		this.originalY = this.targetSprite.y;
		this.tempRect = new FlxRect();	
		this.repeat = repeatAnimation;
		this.defaultDuration = duration;
		reset();
	}
	
	
	
	public function reset():Void
	{
		value = 0;		
		targetSprite.clipRect = this.originalClipRect;
		this.tempRect.x = 0;
		this.tempRect.y = 0;
		this.tempRect.width = 0;
		this.tempRect.height = 0;
		setDuration(this.defaultDuration);		
		started = false;
	}
	
	public function replay(): Void
	{
		reset();
		finished = false;
	}
	
	public function setDuration(duration:Float): Bool
	{
		if (duration <= 0.1)
		{
			return false;
		}
		
		targetIncreaseHeight = targetSprite.height / duration;
		
		return true;
	}
	
	public function isFinished(): Bool
	{
		return finished;
	}
	
	public function isStarted() :Bool
	{
		return started;
	}
	
	public function animate(elapsed:Float)
	{
		started = true;
		if (!finished)
		{
			value += targetIncreaseHeight * elapsed;
			
			if (value > targetSprite.height)
			{
				value = targetSprite.height;			
				finished = true;
			}
			
			switch(scanDirection)
			{
				case SpriteAnim.DOWNAPPEAR:
					this.tempRect.x = 0;
					this.tempRect.y = 0;
					this.tempRect.width = targetSprite.width;
					this.tempRect.height = value;	
					this.targetSprite.y = this.originalY + targetSprite.height - value; 
				case SpriteAnim.UPAPPEAR:
					this.tempRect.x = 0;
					this.tempRect.y = targetSprite.height - value;
					this.tempRect.width = targetSprite.width;
					this.tempRect.height = value;		
					this.targetSprite.y = this.originalY - targetSprite.height + value; 
				case SpriteAnim.DOWNDISAPPEAR:
					this.tempRect.x = 0;
					this.tempRect.y = value;
					this.tempRect.width = targetSprite.width;
					this.tempRect.height = targetSprite.height - value;			
				case SpriteAnim.UPDISAPPEAR:
					this.tempRect.x = 0;
					this.tempRect.y = 0;
					this.tempRect.width = targetSprite.width;
					this.tempRect.height = targetSprite.height - value;			
			}
			
			targetSprite.clipRect = this.tempRect;
			
			if (finished)
			{
				finished = !repeat;
				
				if (finished)
					reset();
			}
		}
		
	}
	

}