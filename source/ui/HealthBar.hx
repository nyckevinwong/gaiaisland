package ui;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxBasic;

/**
 * ...
 * @author ...
 */
class HealthBar
{
	var healthBar: FlxGroup;
	var displayedHP: Int;
	var nextHP:Int;
	var maxHP:Int;
	
	var hearts:Array<Heart> = new Array<Heart>();
	var curHeart:Heart;
	
	public function new()
	{
		displayedHP = 0;
		nextHP = 0;
	}
	
	public function getMaxHP():Int
	{
		return maxHP;
	}
	
	public function create(X:Float, Y:Float):FlxGroup
	{
		
		if (healthBar == null)
		{
			healthBar = new FlxGroup();
			
			var prev:Heart = null;
			var accumHeight: Float = 0;
			
			for (i in 0...5) // add the other 2
			{
				var h: Heart = new Heart(X,  Y + accumHeight);
				
				if (prev != null)
				{
					accumHeight += 14;
				}
		
				h.scrollFactor.set();
				healthBar.add(h);
				hearts[i] = h;
				prev = h;
			}
			
			setMaxHp(4);
		}
		
		return healthBar;
	}
	
	public function setMaxHp(max:Int)
	{
		if (max <= this.hearts.length) 
		{
			this.maxHP = max;
			
			for (i in 0...5) // add the other 2
			{
				hearts[i].visible = true;
				
				if (i > this.maxHP)
				{
					hearts[i].visible = false;
				}
			}
			
		}
	}
	
	public function setHP(hp:Float)
	{
		var newHp:Int  = Std.int(hp);
		
		this.nextHP = newHp;
		
		if (this.nextHP < 0)
		{
			this.nextHP = 0;
		}
		else if (this.nextHP > maxHP)
		{
			this.nextHP = this.maxHP;
		}
	}
	
	public function update(elapsed:Float):Void 
	{ 

		if (nextHP != displayedHP)
		{
			if (nextHP < displayedHP)
			{
				if (curHeart == null || curHeart.animation.finished)
				{					
					var whichHeart: Int = displayedHP;
					
					curHeart = hearts[whichHeart];
					curHeart.animation.play("full-zero");

					displayedHP--;
					
					if (displayedHP < 0)
					{
						displayedHP = 0;
						nextHP = 0;
					}

					FlxG.sound.play(AssetPaths.decreasehp__ogg, 0.5);
				}
				
			}
			else if (nextHP > displayedHP)
			{
				if (curHeart == null || curHeart.animation.finished)
				{
					displayedHP++;
					
					if (displayedHP < 0)
					{
						displayedHP = 0;
						nextHP = 0;
					}
					
					var whichHeart: Int = displayedHP;
					
					curHeart = hearts[whichHeart];
					curHeart.animation.play("zero-full");
					FlxG.sound.play(AssetPaths.increasehp__ogg, 0.5);
				}
			}
			
		}
	}

	
}