package ui;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Heart extends FlxSprite  
{
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		loadGraphic(AssetPaths.hearts__png, true, 12, 14);
		animation.add("empty", [0], 12, false);
		animation.add("half", [0, 1], 30, false);
		animation.add("full", [1, 2], 30, false);
		animation.add("zero-full", [0,1, 2], 6, false);
		animation.add("full-zero", [2,1, 0], 6, false);
		animation.play("empty");
	}
	
}