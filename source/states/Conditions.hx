package states;

import chars.Character;
import chars.FallingBrick;
import chars.GameObject;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import chars.Door;
import chars.PortalVase;
import screenstates.PlayState;
/**
 * ...
 * @author ...
 */
class Conditions
{
	public static function jump(Owner:FlxSprite):Bool
	{
			var char:Character = cast(Owner, chars.Character);
		return screenstates.PlayState.controlPad.isButtonAJustPressed() &&  (Owner.isTouching(FlxObject.DOWN));// || char.isOnLadder());
	}
	
	public static function swimJump(Owner:FlxSprite):Bool
	{
		var char:Character = cast(Owner, chars.Character);
		return screenstates.PlayState.controlPad.isButtonAJustPressed() && char.areAnyTileOverlapped([189]); // jump when on water tiles.
	}
	
	public static function climbJump(Owner:FlxSprite):Bool
	{
		var char:Character = cast(Owner, chars.Character);
		return screenstates.PlayState.controlPad.isButtonAJustPressed() &&  char.isOnLadder();
	}
	
	public static function grounded(Owner:FlxSprite):Bool
	{
		return  Owner.isTouching(FlxObject.DOWN) ;
	}
	
	public static function groundSlam(Owner:FlxSprite):Bool
	{
		/*
		if (Std.is(Owner, Character))
		{
			
			var char:Character = cast(Owner, chars.Character);
			
			
			if (char.isHolding()) // no gourndSlam if it's holding something.
			return false;
			
		}*/
		
		return screenstates.PlayState.controlPad.isButtonBJustPressed() && !Owner.isTouching(FlxObject.DOWN);
	}
	
	public static function animationFinished(Owner:FlxSprite):Bool
	{
		return Owner.animation.finished;
	}
	
	public static function airJump(Owner:FlxSprite):Bool
	{
		return screenstates.PlayState.controlPad.isButtonAJustPressed()  && !Owner.isTouching(FlxObject.DOWN);
	}
	
	public static function climb(Owner:FlxSprite):Bool
	{
		var char:Character = cast(Owner, chars.Character);
		return screenstates.PlayState.controlPad.isUpPressed()  && char.isOnLadder();
	}
	
	public static function offLadder(Owner:FlxSprite):Bool
	{
		var char:Character = cast(Owner, chars.Character);
		return !char.isOnLadder();
	}
	
	
	public static function grab(Owner:FlxSprite):Bool
	{
		if (Std.is(Owner, Character))
		{
			var char:Character = cast(Owner, chars.Character);	
			
			if (char.isGrabbing() || char.isHolding()) // can not switch to grabbing state if a hero is currently grabbing/holding
				return false;
			
		}
		
		return screenstates.PlayState.controlPad.isButtonXJustPressed();
	}
	
	public static function fistAttack(Owner:FlxSprite):Bool
	{
		if (Std.is(Owner, Character))
		{
			var char:Character = cast(Owner, chars.Character);	
			
			if (char.isGrabbing() || char.isHolding()) // can not switch to fist state if a hero is currently grabbing/holding
				return false;
				
		}
		
		return screenstates.PlayState.controlPad.isButtonBJustPressed();
	}
	
	public static function enterPortalVase(Owner:FlxSprite):Bool
	{
		if (screenstates.PlayState.controlPad.isDownJustPressed() ) // if the player is on vase and press down
		{
			var char:Character = cast(Owner, chars.Character);	

			if (char != null)
			{
				if (Std.is(char._onPortal, PortalVase))
					return true;
			}
			
		}		
		
		return false;
	}		
} 

class Attacking extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("attacking");
	}

	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		if (Std.is(owner, Character))
		{
			var char:Character = cast(owner, chars.Character);

			if (char.rideOnTarget == null)
				owner.velocity.x = 0;
		}
	}

}


class Grabbing extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("grabing-from-down");
		if (Std.is(owner, Character))
		{
			var char:Character = cast(owner, chars.Character);
			char.grabTarget();
		}
	}

	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		// unable to be moved while grabbing.
		owner.acceleration.x = 0;
		owner.velocity.x = 0;
	
		// but if it is riding on something
		if (Std.is(owner, Character))
		{
			var char:Character = cast(owner, chars.Character);

			//continue riding if there's anything riddible
			char.perfromRide();	
			
			if(char.animation.curAnim.curIndex >=5)
			char.performGrabbing();
		}
		
	}

}

class EnterPortalVase extends FlxFSMState<FlxSprite>
{
	public var vase: PortalVase;
	
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		var char:Character = null;	
		if (Std.is(owner, Character))
		{
			char = cast(owner, chars.Character);
			vase = cast(char._onPortal, PortalVase);
			char.enterPortal();
		}
		
	}

	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		// unable to be moved while entering.
		owner.acceleration.x = 0;
		owner.velocity.x = 0;
		owner.acceleration.y = 0;
		owner.velocity.y = 0;
		
		if (vase.isTransferingComplete())
		{
			fsm.state = new Idle();
		}
	}
}





class Idle extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("standing");
		owner.acceleration.y =  Character.GRAVITY;
		owner.maxVelocity.set(100, Character.GRAVITY);
	}
	
	override public function exit(owner:FlxSprite):Void 
	{
		var char:Character = cast(owner, chars.Character);
		char.climbing = false;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.acceleration.x = 0; // COMMENT THIS , THEN IT WILL HAVE ICE SKATING EFFECT
		
		var char:Character = null;	
		
		if (Std.is(owner, Character))
		{
			char = cast(owner, chars.Character);
			
			if ( char.areAnyTileOverlapped([189]) ) // if it overlaps any water tile
			{
				var targetGarvity: Float = Character.GRAVITY / 4;
				char.acceleration.y =  targetGarvity;
				char.maxVelocity.set(30, Character.GRAVITY);
			}
			else
			{
				owner.acceleration.y =  Character.GRAVITY;
				char.maxVelocity.set(100, Character.GRAVITY);
			}

		}	
		
				
		if (screenstates.PlayState.controlPad.isButtonBJustPressed() )
		{
			if (char != null)
			{
				char.fireWeapon(); // shot if it's holding a weapon/item
			}
		}
		
		/*

		if (screenstates.PlayState.controlPad.isUpJustPressed() )
		{
			if (char != null)
			{
				if (Std.is(char._onPortal, Door))
					char.enterPortal(); // if it's on a door, try to open door 				
			}
		}
		*/
		
		if (char != null)
		{
			if (Std.is(char._onPortal, Door))
				char.enterPortal(); // if it's on a door, try to open door 				
		}


		if (screenstates.PlayState.controlPad.isLeftPressed() || screenstates.PlayState.controlPad.isRightPressed())
		{
			owner.facing = screenstates.PlayState.controlPad.isLeftPressed() ? FlxObject.LEFT : FlxObject.RIGHT;
			owner.animation.play("walking");
			owner.acceleration.x += screenstates.PlayState.controlPad.isLeftPressed() ? -300 : 300;
		}
		else
		{
			owner.animation.play("standing");			
			owner.velocity.x *= 0.9;
			
			if (char!=null)
			{
				char.perfromRide(); // ride if it's on an enemy.
			}

		}
		
		var standOnTiles: Array<Int> =	char.onWhichTiles();
		
		if (standOnTiles.indexOf(281)!=-1) // if it stands on a spiky
		{
			char.hurt(1);
			FlxG.sound.play(AssetPaths.touchspike__ogg, 1, false);
			fsm.state = new Jump();
		}
		
		if (char != null)
		{
					
			if (char.isTouching(FlxObject.DOWN))
			{
				char.checkBottomHitTiles(
					function(tileX:Int, tileY:Int): Void
					{
						var tileIndex: Int = GameObject.map.getTile(tileX, tileY);
		
						switch(tileIndex)
						{
							case 221:
								GameObject.map.setTile(tileX, tileY, 0, true); // set to an empty tile
								GameObject.addSprite(new FallingBrick(tileX*16,tileY*16));
							default:
						}
					}
				);
			}
		
		}
				
	}
	
}

class Climb extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("standing");
		owner.acceleration.y =  Character.GRAVITY;
		owner.velocity.x = 0; // stop when just jump on ladder
		owner.maxVelocity.set(100, Character.GRAVITY);
	}
	
	override public function exit(owner:FlxSprite):Void 
	{
		var char:Character = cast(owner, chars.Character);
		char.climbing = false;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.acceleration.x = 0;
		
		
		var char:Character = null;	
		var onLadder: Bool = false;
		
		if (Std.is(owner, Character))
		{
			char = cast(owner, chars.Character);
			
			onLadder = char.isOnLadder();
			// fall if it's not on a stair
			
			if (!onLadder)
			char.climbing = false;
			
			owner.acceleration.y = (onLadder && char.climbing ) ? 0 :Character.GRAVITY;			

			if (char.climbing)
				owner.velocity.y = 0;
		}	
		
				
		if (screenstates.PlayState.controlPad.isButtonBJustPressed() )
		{
			if (char != null)
			{
				char.fireWeapon(); // shot if it's holding a weapon/item
			}
		}

		if (screenstates.PlayState.controlPad.isUpJustPressed() )
		{
			if (char != null)
			{
				if (Std.is(char._onPortal, Door))
					char.enterPortal(); // if it's on a door, try to open door 				
			}
		}
		

		if (screenstates.PlayState.controlPad.isLeftPressed() || screenstates.PlayState.controlPad.isRightPressed())
		{
			owner.facing = screenstates.PlayState.controlPad.isLeftPressed() ? FlxObject.LEFT : FlxObject.RIGHT;
			owner.animation.play("walking");
			owner.acceleration.x += screenstates.PlayState.controlPad.isLeftPressed() ? -300 : 300;
		}
		else if (screenstates.PlayState.controlPad.isUpPressed())
		{
			if (onLadder)
			{				
				owner.animation.play("walking");
				owner.velocity.y = -90;
				char.climbing = true;
			}
		}
		else if (screenstates.PlayState.controlPad.isDownPressed() && !owner.isTouching(FlxObject.DOWN))
		{
			if (onLadder)
			{
				owner.animation.play("walking");				
				owner.velocity.y = 90;
				char.climbing = true;
			}			
		}
		else
		{
			owner.animation.play("standing");			
			owner.velocity.x = 0;
			
			if (char!=null)
			{
				char.perfromRide(); // ride if it's on an enemy.
			}

		}
				
	}
	
}

class Jump extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");		
		owner.velocity.y = -300;
		owner.acceleration.y =  Character.GRAVITY;
		
		FlxG.sound.play(AssetPaths.jump__ogg, 1, false);
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		var char:Character = null;	
		if (Std.is(owner, Character))
		{
			char = cast(owner, chars.Character);
			
			if ( char.areAnyTileOverlapped([189]) ) // if it overlaps any water tile
			{
				var targetGarvity: Float = Character.GRAVITY / 4;
				char.acceleration.y =  targetGarvity;
				char.maxVelocity.set(30, Character.GRAVITY);
			}
			else
			{
				owner.acceleration.y =  Character.GRAVITY;
				char.maxVelocity.set(100, Character.GRAVITY);
			}

		}

		
		owner.acceleration.x = 0;
		if (screenstates.PlayState.controlPad.isLeftPressed() || screenstates.PlayState.controlPad.isRightPressed())
		{
			owner.facing = screenstates.PlayState.controlPad.isLeftPressed() ? FlxObject.LEFT : FlxObject.RIGHT;
			owner.acceleration.x = screenstates.PlayState.controlPad.isLeftPressed() ? -400 : 400;
		}

		if (char != null)
		{
		
			if (screenstates.PlayState.controlPad.isButtonBJustPressed() )
			{
					char.fireWeapon();
			}
			
			if (char.isTouching(FlxObject.UP))
			{
				char.checkHeadHitTiles();
			}
		
		}
		
	}
}

class DoubleJump extends Jump 
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -300;
	}
}

class TripleJump extends DoubleJump
{
		override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -300;
	}
	
}

class SuperJump extends Jump
{

	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -390;
	}

}

class SpringJump extends Jump
{

	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("jumping");
		owner.velocity.y = -390;
	}

}

class GroundPound extends FlxFSMState<FlxSprite>
{
	private var _ticks:Float;
	
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("pound");
		owner.velocity.x = 0;
		owner.acceleration.x = 0;
		_ticks = 0;
	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		_ticks++;
		if (_ticks < 15)
		{
			owner.velocity.y = 0;
		}
		else
		{
			owner.velocity.y = GameObject.GRAVITY;
		}
	}
}

class GroundPoundFinish extends FlxFSMState<FlxSprite>
{
	override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		owner.animation.play("landing");
		FlxG.camera.shake(0.025, 0.25);
		owner.velocity.x = 0;
		owner.acceleration.x = 0;
		

	}
	
	override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void 
	{
		
		var char:Character = null;	
		if (Std.is(owner, Character))
		{
			char = cast(owner, chars.Character);
		}

		
		if (char != null)
		{
					
			if (char.isTouching(FlxObject.DOWN))
			{
				char.checkBottomHitTiles();
			}
		
		}
	}
}
