package utils;

import flixel.FlxG;
import flixel.system.scaleModes.FillScaleMode;
import flixel.system.scaleModes.FixedScaleMode;
import flixel.system.scaleModes.RatioScaleMode;
import flixel.system.scaleModes.RelativeScaleMode;

/**
 * ...
 * @author ...
 */
class ScreenUtils 
{
	private var scaleModes:Array<ScreenScaleMode> = [RATIO_DEFAULT, RATIO_FILL_SCREEN, FIXED, RELATIVE, FILL];
	private var scaleModeIndex: Int = 0;
	
	public function new() 
	{		
	}
	
	public function update()
	{
		#if ( !android)
		
		if (FlxG.keys.justPressed.SPACE)
		{
			scaleModeIndex++;

			if (scaleModeIndex >= scaleModes.length)
			scaleModeIndex = 0;
			
			changeScaleModeByIndex(scaleModeIndex);
		}
		
		#end
	}
	
	public function changeScaleModeByIndex(scaleModeIndex:Int): Bool
	{
		if (scaleModeIndex >= 0 && scaleModeIndex < scaleModes.length)
		{
			setScaleMode(scaleModes[scaleModeIndex]);
			return true;
		}
		
		return false;
	}
	
	public function setScaleMode(scaleMode:ScreenScaleMode = ScreenScaleMode.RATIO_DEFAULT)
	{
		FlxG.scaleMode = switch (scaleMode)
		{
			case ScreenScaleMode.RATIO_DEFAULT:
				new RatioScaleMode();
				
			case ScreenScaleMode.RATIO_FILL_SCREEN:
				new RatioScaleMode(true);
				
			case ScreenScaleMode.FIXED:
				new FixedScaleMode();
				
			case ScreenScaleMode.RELATIVE:
				new RelativeScaleMode(0.75, 0.75);
				
			case ScreenScaleMode.FILL:
				new FillScaleMode();
		}
		
		// update index always in case setScaleMode is called directly
		this.scaleModeIndex = scaleModes.indexOf(scaleMode);
	}
}

@:enum
abstract ScreenScaleMode(String) to String
{
	var RATIO_DEFAULT = "ratio";
	var RATIO_FILL_SCREEN = "ratio (screenfill)";
	var FIXED = "fixed";
	var RELATIVE = "relative 75%";
	var FILL = "fill";
}