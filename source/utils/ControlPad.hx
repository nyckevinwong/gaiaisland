package utils;
import flash.display.Bitmap;
import flixel.FlxSprite;
import flixel.ui.FlxVirtualPad;
import flixel.FlxG;
import flixel.group.FlxGroup;

/**
 * ...
 * @author ...
 */
class ControlPad  
{
	public var _vPad: FlxVirtualPad = null;
	
	public function new()
	{
		
	}
	
	public function initControlPad(group:FlxGroup) 
	{
		if (_vPad == null)
		{
			_vPad = new FlxVirtualPad(FULL, A_B_X_Y);
			_vPad.alpha = 0.2;
			//_vPad.width = 1;
			//_vPad.height = 1;
			group.add(_vPad);	
		}
	}
	
	public function showControlPad(show:Bool)
	{
		_vPad.visible = show;
	}
	
	
	public function isUpJustPressed()
	{
		var _up:Bool = _vPad.buttonUp.justPressed; 
		#if( !mobile)
	    _up = _up || FlxG.keys.anyJustPressed([UP]);
		#end
	
		#if(debug_contrlpad)
		trace("UP:" + _up);
		#end
		return _up;
	}

	public function isUpPressed()
	{
		var _up:Bool = _vPad.buttonUp.pressed; 
		#if( !mobile)
	    _up = _up || FlxG.keys.anyPressed([UP]);
		#end
	
		#if(debug_contrlpad)
		trace("UP:" + _up);
		#end
		return _up;
	}
	
	public function isDownJustPressed()
	{
		var _down:Bool  = _vPad.buttonDown.justPressed;
		#if( !mobile)
		_down = _down || FlxG.keys.anyJustPressed([DOWN]);
		#end
		
		#if(debug_contrlpad)
		trace("UP:" + _down);
		#end
		return _down;
	}

	public function isDownPressed()
	{
		var _down:Bool  = _vPad.buttonDown.pressed;
		#if( !mobile)
		_down = _down || FlxG.keys.anyPressed([DOWN]);
		#end
		
		#if(debug_contrlpad)
		trace("UP:" + _down);
		#end
		return _down;
	}
	
	public function isLeftPressed()
	{
		var _left:Bool  = _vPad.buttonLeft.pressed;
		#if( !mobile)
		_left = _left || FlxG.keys.anyPressed([LEFT]);
		#end

		#if(debug_contrlpad)
		trace("left:" + _left);
		#end
		return _left;
	}

	public function isRightPressed()
	{
		var _right:Bool  =  _vPad.buttonRight.pressed;
		#if( !mobile)
		_right =  _right || FlxG.keys.anyPressed([RIGHT]);
		#end
		
		#if(debug_contrlpad)
		trace("_right:" + _right);
		#end
		return _right;
	}
	
	
	public function isButtonAJustPressed()
	{
		var _buttonA:Bool  = _vPad.buttonA.justPressed;
		
		#if ( !mobile)
		_buttonA = _buttonA || FlxG.keys.justPressed.Z; 
		#end
		
		#if(debug_contrlpad)
		trace("_buttonA:" + _buttonA);
		#end
		
		return _buttonA;
	}
	
	public function isButtonBJustPressed()
	{
		var _buttonB:Bool  = _vPad.buttonB.justPressed;
		
		#if ( !mobile)
		_buttonB = _buttonB || FlxG.keys.justPressed.X; 
		#end
		
		#if(debug_contrlpad)
		trace("_buttonB:" + _buttonB);
		#end
		
		return _buttonB;
	}
	
/*
	public function isButtonCJustPressed()
	{
		var _buttonC:Bool  = _vPad.buttonC.justPressed;
		
		#if ( !android)
		_buttonC = _buttonC || FlxG.keys.justPressed.C;
		#end
		
		trace("_buttonC:" + _buttonC);
		
		return _buttonC;
	}
	*/

	public function isButtonXJustPressed()
	{
		var _buttonX:Bool  = _vPad.buttonX.justPressed;
		
		#if ( !mobile)
		_buttonX = _buttonX || FlxG.keys.justPressed.A; 
		#end
		
		#if(debug_contrlpad)
		trace("_buttonX:" + _buttonX);
		#end
		
		return _buttonX;
	}
	
	public function isButtonYJustPressed()
	{
		var _buttonY:Bool  = _vPad.buttonY.justPressed;
		
		#if ( !mobile)
		_buttonY = _buttonY || FlxG.keys.justPressed.S; 
		#end
		
		#if(debug_contrlpad)
		trace("_buttonY:" + _buttonY);
		#end
		
		return _buttonY;
	}
	
	public function isPauseJustPressed()
	{
		var _buttonPause:Bool = false;
		
		#if ( !mobile)
		_buttonPause = _buttonPause || FlxG.keys.justPressed.P; 
		#end

		return _buttonPause;
	}
	
	public function update(elapsed:Float)
	{
		_vPad.update(elapsed);
	}
}