#if android

package utils;

import ru.zzzzzzerg.linden.GoogleIAP;
import ru.zzzzzzerg.linden.iap.PurchaseInfo;
import ru.zzzzzzerg.linden.iap.PurchaseHandler;
import ru.zzzzzzerg.linden.iap.ConnectionHandler;

/**
 * ...
 * @author ...
 */
class InAppPurchase 
{
	private static var IAP_KEY = "GOOGLE_IN_APP_BILLING_KEY";
	public var googleIAP : GoogleIAP;
	
	public var testProduct : PurchaseInfo = null;
	public var managedProduct : PurchaseInfo = null; // real product that costs money
  
	public function new() 
	{
		googleIAP = new GoogleIAP(IAP_KEY, new GoogleIAPHandler(this));
	}
	
}

class GoogleIAPHandler extends ConnectionHandler
{
  var _m : Main;

  public function new(m : Main)
  {
    super();
    this._m = m;
  }

  override public function onServiceCreated(created : Bool)
  {
    super.onServiceCreated(created);

    if(created)
    {
      for(p in _m.googleIAP.getPurchases())
      {
        if(p.productId == "android.test.purchased")
        {
          _m.testProduct = p;
        }
        else if(p.productId == "linden.managed_product")
        {
          _m.managedProduct = p;
        }
      }
    }
    else
    {
      _m.purchaseLog.text.text = "BILLING SERVICE IS NOT CREATED";
    }
  }
}

class IAPPurchaseHandler extends PurchaseHandler
{
  var _m : Main;
  public function new(m : Main)
  {
    super();
    this._m = m;
  }

  override public function purchased(jsonString : String)
  {
    super.purchased(jsonString);

    if(item.productId == "android.test.purchased")
    {
      _m.testProduct = item;
      _m.purchaseLog.text.text = 'Purchased\n${item.productId}';
    }
    else if(item.productId == "linden.managed_product")
    {
      _m.managedProduct = item;
      _m.purchaseLog.text.text = 'Purchased\n${item.productId}';
    }
  }

  override public function finished()
  {
    super.finished();
  }
}

#end