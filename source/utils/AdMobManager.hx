package utils;
import extension.admob.AdMob;
import extension.admob.GravityMode;

/**
 * ...
 * @author ...
 */
class AdMobManager 
{
	public static var AdMobBannerID:String = "ca-app-pub-5295116184266039/9643489373";
	public static var AdMobInterstitialID:String = "ca-app-pub-5295116184266039/4567504087";	

	#if (ADMOBPROD)
	public static var enableAdMobTesting: Bool = false;
	#else
	public static var enableAdMobTesting: Bool = true;
	#end
	
	public static function init()
	{
		if (AdMobManager.enableAdMobTesting)
		{
			// first of all, decide if you want to display testing ads by calling enableTestingAds() method.
			// Note that if you decide to call enableTestingAds(), you must do that before calling INIT methods.
			AdMob.enableTestingAds();
		}

        // if your app is for children and you want to enable the COPPA policy,
        // you need to call tagForChildDirectedTreatment(), before calling INIT.
        // AdMob.tagForChildDirectedTreatment();

        // If you want to get instertitial events (LOADING, LOADED, CLOSED, DISPLAYING, ETC), provide
        // some callback function for this.
        AdMob.onInterstitialEvent = onInterstitialEvent;
		
		AdMob.initAndroid(AdMobManager.AdMobBannerID, AdMobManager.AdMobInterstitialID, GravityMode.TOP); // may also be GravityMode.TOP     
	}
	
	public static function showBanner()
	{
		AdMob.showBanner();
	}
	
	// show ad only after 45-second play
	public static function showInterstitial()
	{
			AdMob.showInterstitial();
	}
		
	
	public static function onInterstitialEvent(event:String) {
        trace("THE INSTERSTITIAL IS "+event);
        /*
        Note that the "event" String will be one of this:
            AdMob.LEAVING
            AdMob.FAILED
            AdMob.CLOSED
            AdMob.DISPLAYING
            AdMob.LOADED
            AdMob.LOADING

        So, you can do something like:
        if(event == AdMob.CLOSED) trace("The player dismissed the ad!");
        else if(event == AdMob.LEAVING) trace("The player clicked the ad :), and we're leaving to the ad destination");
        else if(event == AdMob.FAILED) trace("Failed to load the ad... the extension will retry automatically.");
        */
    }
		
}