package utils;

import flixel.FlxState;
import flixel.util.FlxTimer;
import flixel.FlxG;
import flixel.system.FlxAssets;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class LogoState extends FlxState
{
	private static var NextState:Class<FlxState>;
	private	var logoSprite: FlxSprite; 
	
	public static function init(nextState:Class<FlxState>)
	{
		NextState = nextState;
	}

	override public function create():Void
	{
		super.create();

		this.bgColor = 0xffffffff; // white background
		
		logoSprite = new FlxSprite(0, 0, AssetPaths.logo__png);

		// center the image
		var x:Float = (Main.SCREEN_WIDTH- logoSprite.width) / 2;
		var y:Float = (Main.SCREEN_HEIGHT - logoSprite.height) / 2;
		
		logoSprite.x = x;
		logoSprite.y = y;
		add(logoSprite);

		new FlxTimer().start(1, timerCallback);
		
		#if FLX_SOUND_SYSTEM 
		//FlxG.sound.load(FlxAssets.getSound("flixel/sounds/flixel")).play();
		#end
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	private function timerCallback(Timer:FlxTimer):Void
	{
		if (logoSprite != null) logoSprite.destroy();		
		logoSprite = null;
		
		var _requestedState:FlxState = cast Type.createInstance(NextState, []);
		FlxG.switchState(_requestedState);
	}	
}