package utils;

import flixel.util.FlxSave;
import haxe.ds.IntMap;
/**
 * ... 
 * @author Kevin Wong
 */
class GameSave 
{
	private static var gameSave:FlxSave = new FlxSave();
	private static var firstTime: Bool = true;
	
	public static function read(): FlxSave
	{
		if (GameSave.firstTime)	
		{
			GameSave.gameSave.bind("gaiaisland");
			if (GameSave.gameSave.data.unlockedLevels == null)
			{
				GameSave.gameSave.data.unlockedLevels = new Array<Bool>();
				
				// 5 levels per a big level
				// total 8 big levels, 40 levels in total
				
				GameSave.gameSave.data.unlockedLevels.push(true); // always unlock first level
				
				for (i in 0...40) // locked all other 39 levels.
				{
					GameSave.gameSave.data.unlockedLevels.push(false);
				}
			}
		}
		
		GameSave.firstTime = false;
		return GameSave.gameSave;
	}
		
	public static function destroy() 
	{
		GameSave.gameSave.destroy();
	}
	
}