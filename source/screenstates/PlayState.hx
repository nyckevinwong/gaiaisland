package screenstates;

import chars.Door;
import chars.PatrolGuy;
import chars.Portal;
import chars.PortalVase;
import chars.GameObject;
import chars.Character;
import chars.Coin;
import chars.Hero;
import chars.Barbarian;
import chars.Item;
import chars.Slime;
import chars.PatrolEnemy;
import chars.Bullet;
import chars.Spring;
import chars.TemporaryInfo;
import chars.boss.Devil;
import chars.Elevator;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;

import flixel.util.FlxStringUtil;
import flixel.util.FlxPath;

import maputils.Areas;
import maputils.Rect;
import maputils.Levels;
import maputils.generator.GenerateMapState;
import maputils.tile.AnimatedTiledMap;
import ui.HealthBar;

import observers.PortalObserver;

import flash.display.InterpolationMethod;
import utils.GameSave;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.tile.FlxBaseTilemap.FlxTilemapAutoTiling;
import flixel.util.FlxColor;

	
import flixel.group.FlxGroup;
import flixel.effects.particles.FlxEmitter;

import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledLayer.TiledLayerType;
import flixel.addons.tile.FlxTilemapExt;

import flixel.system.scaleModes.FillScaleMode;
import flixel.system.frontEnds.VCRFrontEnd;

import utils.ControlPad;
import utils.AdMobManager;
import utils.ScreenUtils;

import utils.LogoState;

import maputils.tile.FlxAnimatedTilemapExt;

//import sys.io.File;

using StringTools;



class PlayState extends FlxState
{
	private var _mapLoader:AnimatedTiledMap;
	private var _map:FlxAnimatedTilemapExt;
	private var _hero:chars.Character;
	private var _powerup:FlxSprite;
	
	private var _gibs:FlxEmitter;
	private var _blockgibs:FlxEmitter;
	
	private var _info:String = "Current State: {STATE}";
	private var _txtInfo:FlxText;
	public static var controlPad: ControlPad;

	private var _items: FlxGroup= new FlxGroup();
	private var _enemies: FlxGroup= new FlxGroup();
	private var _heros: FlxGroup= new FlxGroup();
	private var _bosses: FlxGroup= new FlxGroup();
	private var _doors: FlxGroup = new FlxGroup();
	private var _frontMapLayer:FlxGroup = new FlxGroup();
	private var _portalbottles: FlxGroup= new FlxGroup();
	private var _ui: FlxGroup = new FlxGroup();
	
	private var areas:Areas = new Areas();
	private var bossArea:Rect=null;
	private var portalObserver: PortalObserver=null;
	private static var levels :Levels = new Levels();
		
	private var _bullets:FlxTypedGroup<Bullet>;
	
	private var screenSacleMode:Int = 0;
	private var tempInfo :TemporaryInfo = new TemporaryInfo();
	private var isBossReady:Bool = false;
	private var isBossDead:Bool = false;
	private var bossDeadElapsed:Float = 0;
	private var boss: Devil;
	private var healthBar: HealthBar = new HealthBar();
	private var textGameOver:FlxText;
	private var pauseGame: Bool = false;
	private var pauseState: PauseState;
	
	public static var screenUtils: ScreenUtils = new ScreenUtils();
	private static var count:Int = 0;
	
	override public function create():Void
	{	
		super.create();
		
		Character.addItemCallBack = this.addItem;
		GameObject.addSprite = this.addSprite;
		Item.updateMaxHealthCallBack = this.updateMaxHealth;

		count++;
		if (count <= 1)
		{
			FlxG.resetState();
 
		}
		
		
		
		bgColor = 0xFF429AD7;

		screenUtils.setScaleMode();
		
		_mapLoader = new AnimatedTiledMap(Levels.current());
		 _map = new FlxAnimatedTilemapExt();
		 
		 var tileLayer: TiledTileLayer = cast(_mapLoader.getLayer("walls"), TiledTileLayer);
		 _map.loadMapFromArray(tileLayer.tileArray, tileLayer.width, tileLayer.height, AssetPaths.tiles__png, _mapLoader.tileWidth, _mapLoader.tileHeight, FlxTilemapAutoTiling.OFF, 1, 1, 200);
		
		 _mapLoader.loadAnimationForTiledTileLayer(_map, tileLayer);
	//	 var csvData:String = FlxStringUtil.bitmapToCSV(GenerateMapState.mapData);
//		 _map.loadMapFromCSV(csvData, AssetPaths.tiles__png, 16, 16, FlxTilemapAutoTiling.OFF,1, 1, 200);
		 
		 
		 _map.setTileProperties(1, FlxObject.UP | FlxObject.LEFT);
		 _map.setTileProperties(2, FlxObject.UP);
		 _map.setTileProperties(3, FlxObject.UP | FlxObject.RIGHT);
		 _map.setTileProperties(21, FlxObject.LEFT);
		 _map.setTileProperties(23, FlxObject.RIGHT);
		 _map.setTileProperties(41, FlxObject.DOWN | FlxObject.LEFT);
		 _map.setTileProperties(42, FlxObject.DOWN);
		 _map.setTileProperties(43, FlxObject.DOWN | FlxObject.RIGHT);
		 
		 _map.setTileProperties(4, FlxObject.UP | FlxObject.LEFT);
		 _map.setTileProperties(5, FlxObject.UP);
		 _map.setTileProperties(6, FlxObject.UP | FlxObject.RIGHT);
		 _map.setTileProperties(24, FlxObject.LEFT);
		 _map.setTileProperties(26, FlxObject.RIGHT);
		 _map.setTileProperties(44, FlxObject.DOWN | FlxObject.LEFT);
		 _map.setTileProperties(45, FlxObject.DOWN);
		 _map.setTileProperties(46, FlxObject.DOWN | FlxObject.RIGHT);
		 
		 _map.setTileProperties(7, FlxObject.UP | FlxObject.LEFT);
		 _map.setTileProperties(8, FlxObject.UP);
		 _map.setTileProperties(9, FlxObject.UP | FlxObject.RIGHT);
		 _map.setTileProperties(10, FlxObject.UP | FlxObject.LEFT);
		 _map.setTileProperties(11, FlxObject.UP);
		 _map.setTileProperties(12, FlxObject.UP | FlxObject.RIGHT);
		 _map.setTileProperties(13, FlxObject.UP | FlxObject.LEFT);
		 _map.setTileProperties(14, FlxObject.UP);
		 _map.setTileProperties(15, FlxObject.UP | FlxObject.RIGHT);

 		 _map.setTileProperties(30, FlxObject.LEFT);
		 _map.setTileProperties(32, FlxObject.RIGHT);

		 
		 _map.follow();
		
		textGameOver = new FlxText(0, Main.SCREEN_HEIGHT/3, FlxG.width, "Game Over");
		textGameOver.setFormat(null, 50, FlxColor.YELLOW, CENTER, OUTLINE, FlxColor.BLACK);
		textGameOver.visible = false;
		textGameOver.antialiasing = true;
		textGameOver.scrollFactor.set(0, 0);
		// Add last so it goes on top, you know the drill.
		
		// Set up the gibs
		_gibs = new FlxEmitter();
		_gibs.velocity.set(-150, -200, 150, 0);
		_gibs.angularVelocity.set( -720, 720);
		_gibs.loadParticles(AssetPaths.lizgibs__png, 25, 16, true);
		
		_blockgibs = new FlxEmitter();
		_blockgibs.acceleration.start.min.y = 0;
		_blockgibs.acceleration.start.max.y = 0;
		_blockgibs.acceleration.end.min.y = 500;
		_blockgibs.acceleration.end.max.y = 800;
		_blockgibs.loadParticles(AssetPaths.redblockgibs__png, 4, 16, true);

		
		// ADD BULLETS
		// Create the actual group of bullets here
		_bullets = new FlxTypedGroup<Bullet>();
		_bullets.maxSize = 4;
		
		// Set up the individual bullets
		// Allow 4 bullets at a time
		for (i in 0...4)
		{
			_bullets.add(new Bullet());
		}

		Portal.clearOversables(); // clear memory leak issue related to obversable.
		
		// this is created after bullets are available
		loadObjects(_mapLoader.layers);

		// connect area to related class objects
		portalObserver = new PortalObserver(areas);
		Portal.addObserver(portalObserver);
		
		bossArea = areas.get("boss");
		boss.bossArea = bossArea;
		
		
		GameObject.map = _map;
		GameObject.tileLayer = tileLayer;


	//	_map.follow();
		
	portalObserver.updateArea(this.areas);
	portalObserver.update(null, _hero);
			// set up current scrollbound and worldbound by hero's init position
	/*		var rect: Rect = areas.containsPoint(_hero.x, _hero.y);
			if (rect != null)
			{
				FlxG.camera.setScrollBoundsRect(rect.left , rect.top, rect.width, rect.height, false);
				GameObject.activeArea = rect;
			}
*/
		// add hearts
		
		_ui.add(healthBar.create(5,30));
		healthBar.setMaxHp(3);
		
		
		// layer order
		add(_map);
		add(_doors);
		add(_frontMapLayer);
		add(_enemies);
		add(_bosses);
		add(_items);
		add(_powerup);
		add(_bullets); 
		add(_heros);
		add(_portalbottles);
		add(textGameOver);
		add(_gibs);
		add(_blockgibs);
		add(_ui);
		
		_txtInfo = new FlxText(16, 32, -1, _info);
		_txtInfo.scrollFactor.set();
		
		_txtInfo.visible = false;
		
		add(_txtInfo);
		
		add(tempInfo);
		
		
		
		controlPad = new ControlPad();
		controlPad.initControlPad(this);
		controlPad.showControlPad(true);
		
		#if (windows)
		controlPad._vPad.visible = false; //hide by default for windows version
		#else

		controlPad._vPad.visible = true;
		// hidden by default
		controlPad._vPad.buttonX.visible = false;
		controlPad._vPad.buttonY.visible = false;
		
		controlPad._vPad.buttonUp.visible = true;
		controlPad._vPad.buttonDown.visible = true;
		
		#end
	
		FlxG.sound.playMusic(AssetPaths.scrollingspace__ogg, 0.5);

		
		AdMobManager.showBanner();
		
	}
	
	public function updateMaxHealth(maxHealth:Float)
	{
		_hero.maxHealth = maxHealth;
		healthBar.setMaxHp(Std.int(maxHealth));
	}
	
	public function adjustInitYPosition(sprite:FlxSprite, MapY: Int )
	{
		sprite.y = MapY + 16 - sprite.height;		
	}
	
	public function createHero(x:Int, y:Int)
	{
		if (_hero != null)
		{
		_hero.destroy();
		_heros.remove(_hero);
		}
			
		_hero = new Hero(x, y);
		adjustInitYPosition(_hero, y);
		
		_hero.health = 3;
		_hero.setDeathEmitter(_gibs);
		_hero.setBlockEmitter(_blockgibs);
		_heros.add(_hero);
		
		_hero.tempInfo = new TemporaryInfo();
		_ui.add(_hero.tempInfo);
		
		_hero.tempInfo.color = FlxColor.WHITE;
		_hero.tempInfo.setBorderStyle(SHADOW, FlxColor.BLACK);
		FlxG.camera.follow(_hero, PLATFORMER, 1);
			
		_hero.setBulletPool(_bullets);

	}
	
	public function loadObjects(layers:Array<TiledLayer>)
	{
		for (layer in layers)
		{
			if (layer.type == TiledLayerType.OBJECT)
			{
				var objectLayer:TiledObjectLayer = cast layer;
				processTieldObjectLayer(objectLayer);
			}
			else if (layer.type == TiledLayerType.TILE)
			{
				var tileLayer : TiledTileLayer = cast layer;
				processTileLayer(tileLayer);
			}

		}
	}
	
	public function processTileLayer(layer:TiledTileLayer)
	{
		
	}
	
	public function processTieldObjectLayer(layer:TiledObjectLayer)
	{
			//objects layer
			if (layer.name == "objects")
			{
				for (o in layer.objects)
				{
					loadObject(o, layer);
				}
			}		
	}
	
	private function loadObject(o:TiledObject, g:TiledObjectLayer)
	{
		var x:Int = o.x;
		var y:Int = o.y;
		var width:Int =	o.width;
		var height: Int = o.height;
		
		// objects in tiled are aligned bottom-left (top-left in flixel)
		if (o.gid != -1)
			y -= g.map.getGidOwner(o.gid).tileHeight;

		switch (o.type.toLowerCase())
		{
			case "area":
				var name: String = o.name;		
				var rect: Rect = new Rect(x, y, width, height);
				rect.name = name;
				areas.add(name, rect);
			case "boss":
				boss = new Devil(x, y);	
				
				adjustInitYPosition(boss, y);				
				_bosses.add(boss);
			case "coin":
//				var tileset = g.map.getGidOwner(o.gid);
				var coin: Coin = new Coin(x, y);
				_items.add(coin);
			case "bluepatrol":
				var bluePatrol: PatrolEnemy = new PatrolEnemy(x, y);
				bluePatrol.allowCollisions = FlxObject.UP | FlxObject.DOWN;
				_enemies.add(bluePatrol);
			case "greenpatrol":
				var greenPatrol: PatrolEnemy= new PatrolEnemy(x, y, 1);
				greenPatrol.allowCollisions = FlxObject.UP | FlxObject.DOWN;
				_enemies.add(greenPatrol);
			case "apple":
				var apple:Item = new Item(x, y, Item.APPLE);
				_items.add(apple);
			case "chess":
				var chess:Item = new Item(x, y, Item.CHESS);
				_items.add(chess);
			case "shotgun":
				var shotgun:Item = new Item(x, y, Item.SHOTGUN);
				_items.add(shotgun);
			case "hero":
				createHero(x, y);
			case "pizza":
				// ADD POWERUP
				_powerup = new FlxSprite(x, y, AssetPaths.powerup__png);
			case "door":				
				var portalID:Int = Std.parseInt(o.name);			
				var portal:Door = new Door(portalID, x, y);
				var disappearAfterEnter: String = o.properties.get("disappearAfterEnter");				
				_doors.add(portal);
				
			//	trace("portID:" + portalID + " => + disappearAfterEnter "+ disappearAfterEnter);
				if (disappearAfterEnter!=null && disappearAfterEnter == "true")
				{
					
					portal.disappearAfterEnter = true;
				}
				
			case "portalvase":
				var portalID:Int = Std.parseInt(o.name);
				var portal: PortalVase= new PortalVase(portalID, x, y);
				_portalbottles.add(portal);
			case "elevator":
				if(o.points.length >= 1)
				{
					var e : Elevator = new Elevator(o.x, o.y);
					var strSpeed:String = o.properties.get("speed");
					var speed:Float = (strSpeed == null) ? 40: Std.parseFloat(strSpeed);
					
					var array: Array<FlxPoint> = new Array<FlxPoint>();
					
					for (point in o.points) {
						array.push(new FlxPoint(o.x + point.x, o.y + point.y));
					}
					
					e.path = new FlxPath().start(array, speed , FlxPath.YOYO);
					e.allowCollisions = FlxObject.UP;
					
					_frontMapLayer.add(e);
				}
			case "spring":
				var spring: Spring = new Spring(x, y);
				spring.hero = _hero;
				_frontMapLayer.add(spring);
				
		}
	}
	
	
	public function MapCollideCheck()
	{
		var g:FlxGroup = new FlxGroup();
		g.add(_heros);
		g.add(_enemies);
		g.add(_frontMapLayer);
	//	g.add(_bosses); //boss only collide with hero, activeArea, not the map.
		g.add(_bullets);
		g.add(_doors);// door can fall
		g.add(_items);
		
		FlxG.collide(_map, g);
	}

		
	override public function update(elapsed:Float):Void
	{		
		// must render super.update first before other update. this fixed black screen issue.
		// this call fixed double just-pressed buttion issue when there is only one just-pressed. 
		// this call should be placed after any virtual pad key/button check
		// Button X & Y are NOT working!!
			
		
		/* temp fix to test area issue related to memory leak
		if (FlxG.keys.justPressed.F)
			{
					portalObserver.updateArea(this.areas);
					portalObserver.update(null, _hero);
			}*/
		
		#if (html5 || windows)
		if (FlxG.keys.justPressed.ESCAPE)
		{
			FlxG.sound.pause();
			FlxG.switchState(new LogoState());
		}
		else if (FlxG.keys.justPressed.B)
		{
			controlPad._vPad.visible = !controlPad._vPad.visible;
			if (controlPad._vPad.visible)
			{
				// hidden by default
				controlPad._vPad.buttonX.visible = false;
				controlPad._vPad.buttonY.visible = false;
				
				controlPad._vPad.buttonUp.visible = true;
				controlPad._vPad.buttonDown.visible = true;
			}
		}
		#end
			
		#if (windows)
		
		else if (FlxG.keys.justPressed.F5)
		{
			FlxG.vcr.startRecording(false);
		}
		else if (FlxG.keys.justPressed.F6)
		{
			var data: String  = FlxG.vcr.stopRecording(false);
		//	sys.io.File.saveContent("./record.fgr", data);
		}
		else if (FlxG.keys.justPressed.F7)
		{
			FlxG.vcr.loadReplay("./record.fgr", new PlayState(), [], 30, onDemoComplete);
		}
		else if (FlxG.keys.justPressed.I)
		{
			_txtInfo.visible = !_txtInfo.visible;
		}
		
		#end
		

		if (controlPad.isPauseJustPressed())
		{
			pauseGame = !pauseGame;
			
			
			if (pauseGame)
			{
				if (pauseState != null)
					remove(pauseState);
					
				pauseState = new PauseState();
				add(pauseState);
			}
			else
			{
				remove(pauseState);
			}

		}
			
		if (pauseGame)
		{
			_txtInfo.text = "Pause. Press P to continue the game.";
			controlPad.update(elapsed); 
		}
		else
		{
			
			if (isBossDead)
			{
				bossDeadElapsed += elapsed;
				
				if (bossDeadElapsed >= 5)
				{
						bossDeadElapsed = 0;
						nextGame();
				}
			}
			else
			{
					
					
				#if (windows)

				_txtInfo.text = _info.replace("{STATE}", Type.getClassName(_hero.fsm.stateClass));
				
				_txtInfo.text += "Testing Ad :" + AdMobManager.enableAdMobTesting + " overlapTiles:" +_hero.overlapWhichTiles();
				_txtInfo.text += "\nhero(" + Std.int(_hero.x) + "," + Std.int(_hero.y) + ")";
				var standOnTiles: Array<Int> = _hero.onWhichTiles();
				_txtInfo.text += "\n stand-on tiles: " +  standOnTiles[0] + ","  + standOnTiles[1];
				_txtInfo.text += "\n Area: " + areas.getName(GameObject.activeArea);
								
				
				var r: Rect = GameObject.activeArea;
				_txtInfo.text += " (" + r.x + "," + r.y + "," + r.width  + "," +  r.height +")";

				_txtInfo.text += "\n Area List: "  + areas.getInfo();
				
				#end
						
				MapCollideCheck();
				
				// reset these character attibutes for every characters and every update
				_hero.ride(null);
				_hero._onPortal = null; 

				FlxG.collide(_hero, _frontMapLayer);
				FlxG.collide(_hero, _enemies, collideEnemy);
				FlxG.collide(_hero, _portalbottles, collideBottles);
				FlxG.overlap(_hero, _enemies, monsterAttack, processMonsterAttack);
				FlxG.overlap(_hero, _bosses, monsterAttack);
				FlxG.overlap(_hero, _powerup, getPowerup);
				FlxG.overlap(_hero, _items, collectItem);
				FlxG.overlap(_hero, _doors, onDoor);
				FlxG.overlap(_bullets, _enemies, hitmonster);
				FlxG.overlap(_bullets, _bosses, hitmonster);
				
			//	_hero.performHolding();
				
				#if ( !android)
				
				if (FlxG.keys.justPressed.R)
				{
					restartGame();
				}
				
				
				#end
				
				
				BossReadyCheck();		
				healthBar.setHP(_hero.health);
				
				healthBar.update(elapsed);
				
				if (_hero.health == 1 )
				{
					//FlxG.sound.load(AssetPaths.lowhealthalarm__ogg, 1, true);
				}
				
				if (_hero.health <= 0 && _hero.exists == false)
				{
					textGameOver.visible  = true;
					controlPad._vPad.visible = false;
					
					#if ( !android)
				
					if (FlxG.keys.justPressed.ANY)
					{
						restartGame();
					}
					
					#end
					
					for (touch in FlxG.touches.list)
					{
						if (touch.justPressed) { restartGame();  }
					}
				}
			
			screenUtils.update();
			// fsm.stateClass only exist after super.update() call

			controlPad.update(elapsed); 
			super.update(elapsed);
			
			}

	
		}
		
	}
	
	public function nextGame()
	{
			// switch level
			
			Levels.next(); // go to next level. if no more level, go back to the first level.
			AdMobManager.showInterstitial(); // disable Interstitial ad when restaring a game	
			areas.destroy();
			FlxG.camera.flash(FlxColor.BLACK, .1, 
			function():Void
			{
			FlxG.switchState(new LevelSelectorState());
			});		
	}
	
	public function restartGame(showAds:Bool=true)
	{
				if (showAds)
				{
					AdMobManager.showInterstitial(); // disable Interstitial ad when restaring a game					
				}
					
				areas.destroy();
				FlxG.camera.flash(FlxColor.BLACK, .1, FlxG.resetState);		
	}
	
	private function onDoor(char:chars.Character, door: Door)
	{
		// update door when it's on door.
		if (char._onPortal != door)
		{
			char._onPortal = door;
		}
		
	}
	
	private function monsterAttack(char:FlxObject, Monster:FlxObject):Void
	{
		char.hurt(1);
	}
	
	private function processMonsterAttack(char:FlxObject, Monster:FlxObject):Bool
	{
		if (Monster.isTouching(FlxObject.UP) )
		return false;
		
		var charY:Float = char.y + char.height;
		
		// import feature: if the bottom hits the top 5 pixel of the head of a monster, ignore this collision.
		if (charY <= Monster.y+4) 
		return false;
		
		return true;
	}
	
	private function hitmonster(Blt:FlxObject, Monster:FlxObject):Void 
	{
		if (!Monster.alive) 
		{
			// Just in case
			return;
		}
		
		if (Monster.health > 0) 
		{
			Blt.kill();
			Monster.hurt(1);
			tempInfo.x = Monster.x + Monster.width / 2;
			tempInfo.y = Monster.y;
			tempInfo.text = "-1";
			tempInfo.displayInfo();

		}
	}
	
	private function BossReadyCheck()
	{
		if (!isBossDead) // no musich switch after the boss is dead.
		{
			var overlap: Bool = bossArea.overlapsByFlxRect(_hero.getHitbox());
			
			if (overlap && !isBossReady )
			{
				isBossReady = true;
				
				FlxG.sound.playMusic(AssetPaths.fight__ogg, 0.5);
				
			}
			else if (!overlap && isBossReady)
			{
				isBossReady = false;
				FlxG.sound.playMusic(AssetPaths.scrollingspace__ogg, 0.5);			
			}
		}
		
		if (!isBossDead && boss.isDead)
		{
			FlxG.sound.playMusic(AssetPaths.victory__ogg, 1 , false); // only place once			
			isBossDead = true; // boss is dead already
		}
		
	}
	
	private function collideBottles(char:chars.Character, p: Portal):Void
	{
		if (p.isTouching(FlxObject.UP))
		{
			char._onPortal = p;
		}				
	}
	
	private function collideEnemy(char:chars.Character, p: GameObject):Void
	{
		_txtInfo.text = "Left/Right to Move. Z or B = Jump, X or A = Grab.";
		
		if (char.isGrabbing())
		return;
				
		#if (debug)		
		_txtInfo.text += "\n colliding enemy:" + p.x + "\n," + p.y + "\n => " + ( char.y + char.height) ;
		#end

		if (p.isTouching(FlxObject.UP))
		{
			if (p.isRiddible && p.y == (char.y + char.height))
			{
				char.ride(p);
			}
		}
		
		
	}
	
	private function getPowerup(char:chars.Character, particle:FlxSprite):Void
	{		
		//slime.fsm.transitions.replace(Slime.Jump, Slime.SuperJump);
		//slime.fsm.transitions.add(Slime.Jump, Slime.Idle, Slime.Conditions.grounded);
		if (char.isHoldingItem(Item.SHOTGUN))
		{			
			var item : Item  = cast(char.holdingTarget, Item);
			char.throwObject();
			_items.remove(item);
			
			_items.add( new Item(350, 464, Item.SHOTGUN) );			
		}
		
		switchCharacter();
		
		if (particle.x == 416)
		{
		particle.x = 496;
		}
		else
		{
		particle.x = 416;
		}
		
	}
	
	private function collectItem(char:chars.Character, sprite:FlxSprite):Void
	{		
		if (Std.is(sprite, Item ))
		{
			var item: Item = cast(sprite, Item);
			item.use(char);
		}
		else
		{
			sprite.kill();
		}
	}
	
	public function switchCharacter()
	{
		var x: Float = _hero.x;
		var y: Float = _hero.y;
		var hp: Float = _hero.health;
		
		var holdingTarget: FlxSprite = _hero.holdingTarget; 
		_hero.destroy();
		
		_heros.remove(_hero);
		
		if (Std.is(_hero, PatrolGuy))
		{
			_hero = new chars.Hero(x, y-8);
		}
		else if (Std.is(_hero, chars.Hero))
		{
			_hero = new chars.PatrolGuy(x, y);
		}
			_hero.holdingTarget = holdingTarget;
			_hero.setBulletPool(_bullets);
			_hero.health = hp;
			_hero.setDeathEmitter(_gibs);
		
		_heros.add(_hero);
		FlxG.camera.follow(_hero, PLATFORMER, 1);
				
	}

	public function addItem(item:Item):Void
	{
		_items.add(item);
	}

	override public function destroy():Void {		
		super.destroy();
	}
	
	public function addSprite(sprite: FlxSprite):Void
	{
		_frontMapLayer.add(sprite);
	}
	
		/**
	 * This function is called by FlxG.loadReplay() when the replay finishes.
	 * Here, we initiate another fade effect.
	 */
	private function onDemoComplete():Void
	{
		FlxG.cameras.fade(0xff131c1b, 1, false, onDemoFaded);
	}
	
	/**
	 * Finally, we have another function called by FlxG.fade(), this time
	 * in relation to the callback above.  It stops the replay, and resets the game
	 * once the gameplay demo has faded out.
	 */
	private function onDemoFaded():Void
	{
		FlxG.vcr.stopReplay();
		FlxG.resetGame();
	}
}
