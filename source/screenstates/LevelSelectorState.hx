package screenstates;
import haxe.xml.Fast;
import flixel.FlxG;
import flixel.addons.ui.FlxUIState;
import flixel.addons.ui.FlxUIButton;
import flixel.addons.ui.FlxUIText;
import openfl.Assets;
import openfl.events.Event;
import maputils.Levels;
import haxe.ds.IntMap;

using flixel.util.FlxStringUtil;

/**
 * ...
 * @author Lars Doucet
 */
class LevelSelectorState extends FlxUIState
{
	/*
	public var reload_ui_on_asset_change(default, set):Bool;

	private function set_reload_ui_on_asset_change(b:Bool):Bool {
	// whether or not to reload UI when assets are updated
	if (b) Assets.addEventListener(Event.CHANGE, reloadUI);
	else Assets.removeEventListener(Event.CHANGE, reloadUI);
	return reload_ui_on_asset_change = b;
	}
		*/
	private var buttons: IntMap<FlxUIButton> = new IntMap<FlxUIButton>();
	private var curPage: Int = 0;
	
	override public function create() 
	{
		//this.reload_ui_on_asset_change = true;
		_xml_id = "levelselector_state";
		super.create();
	}
	
	
	private function gotoTargetLevel(index:Int)
	{
		Levels.setLevel(index);		
		FlxG.switchState(new PlayState());
	}
	
	private function nextPage()
	{
		curPage++;
		
		var maxPage: Int = Std.int(Levels.totalLength() / 10);
		
		if (curPage > maxPage)
		curPage = maxPage;
	}
	
	private function prevPage()
	{
		curPage--;
		if (curPage < 0)
		curPage = 0;
	}
	
	private function initCurrentPage(beginIndex:Int) 
	{		
		for(i in 1...11)
		{
			var button:FlxUIButton =  buttons.get(i);
			var textField: FlxUIText = button.label;
			var selIndex: Int = beginIndex + i - 1; // convert to zero-based index
			var label:String = Levels.getTitle(selIndex);
			
			if (label != null)
			{
				textField.text = label;
				button.visible = true;
							
				if (selIndex > Levels.getMaxTestLevel())
				{	// when determining a level is locked/not, ignore testing levels.					
					var levelIndex: Int = selIndex - Levels.getMaxTestLevel();
					button.visible = Levels.isLevelUnlocked(levelIndex);
				}
			}
			else
			{
				button.visible = false;
			}
			
		}
	}
	
	public override function getEvent(name:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void
	{
		//trace("LevelSelector: getEvent:" + name + " sender:" + sender + " data:" + data + " params:" + params);
		
			switch (name)
			{
				case "finish_load":
						
					for(i in 1...11)
					{
						var buttonId: String = "option" + i + "_button";
						var button:FlxUIButton = cast _ui.getAsset(buttonId);
						buttons.set(i, button);
						
						trace(button.name);
					}
					
					initCurrentPage(0);
				
				case "click_button":
					var selectedIndex: Int = curPage * 10 + Std.int(params[0]) - 1;

					switch (Std.string(params[0]))
					{
						case "back": FlxG.switchState(new TitleState());
						default:
							gotoTargetLevel(selectedIndex);
					}
			}
	}
	
}