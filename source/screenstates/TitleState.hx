package screenstates;

import flash.text.TextField;
import flixel.FlxG;
import flixel.addons.ui.FlxUIButton;
import flixel.addons.ui.FlxUISprite;
import flixel.addons.ui.FlxUIState;
import flixel.addons.ui.FlxUIRadioGroup;
import flixel.addons.ui.FlxUICheckBox;
import flixel.addons.ui.FlxUIText;
import flixel.text.FlxText.FlxTextFormat;
import flixel.text.FlxText.FlxTextBorderStyle;
import flixel.FlxG;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import openfl.Assets;
import utils.FireTongueEx;
import flixel.FlxSprite;
import maputils.MapUtils;
import flixel.tile.FlxBaseTilemap.FlxTilemapAutoTiling;

import openfl.Lib.getURL;
import openfl.net.URLRequest;

/**
 * ...
 * @author Kevin Wong
 */
class TitleState  extends FlxUIState
{
	

	override public function create():Void
	{
		bgColor = 0xFF429AD7;
		
		_xml_id = "state_title";

		var map: FlxTilemap = MapUtils.LoadFlxTilemap(AssetPaths.title__tmx, FlxTilemapAutoTiling.OFF, 1, 1);
		
		add(map);
		
		if(FlxG.sound.music==null || !FlxG.sound.music.playing)
			FlxG.sound.playMusic(AssetPaths.titlescreen__ogg, 0.5);

			
		super.create();

	}
	
	private function formatText(textbox:FlxUIText)
	{

		var format1 = new FlxTextFormat(0xE6E600, false, false, FlxColor.WHITE);
		var format2 = new FlxTextFormat(0xFFAE5E, false, false, FlxColor.WHITE);
		var format3 = new FlxTextFormat(0x008040, false, false, FlxColor.WHITE);
		var format4 = new FlxTextFormat(0x0080C0, false, false, FlxColor.WHITE);
		var format5 = new FlxTextFormat(0x00E6E6, false, false, FlxColor.WHITE);
		var format6 = new FlxTextFormat(0x0080FF, false, false, FlxColor.WHITE);
		
		var inc: Int = Std.int(Math.ceil(textbox.text.length / 6)) ;

		trace("game title length:" + textbox.text.length + ", increase:" + inc );

		if (Main.tongue.locale == "zh-TW")
		{
			inc = Std.int(Math.ceil(inc / 2));
		}

		if (inc < 1) inc = 1;
		
		
		textbox.setBorderStyle(FlxTextBorderStyle.SHADOW, FlxColor.WHITE, 2);

		textbox.clearFormats();
		textbox.addFormat(format1, 0, inc);
		textbox.addFormat(format2, inc, 2*inc);
		textbox.addFormat(format3, 2*inc, 3*inc);

		if (textbox.text.length <= 4)
		{
			textbox.addFormat(format4, 3*inc, textbox.text.length);
		}
		else if (textbox.text.length <= 5)
		{
			textbox.addFormat(format4, 3*inc, 4*inc);
			textbox.addFormat(format5, 4*inc, textbox.text.length);
		}
		else
		{
			textbox.addFormat(format4, 3*inc, 4*inc);
			textbox.addFormat(format5, 4*inc, 5*inc);
			textbox.addFormat(format6, 5*inc, textbox.text.length);
		}
	}
	
	private function initFont(target:String, defaultFontName:String)
	{
		var text:FlxUIText = cast _ui.getAsset(target);
			var fontName: String = Main.tongue.getFont(defaultFontName);
		
		trace("target:" + target + " -> initFont");
		
		if (text != null)
		{
			text.setFormat(fontName, text.size);
			trace("TextSize:" + text.size);
		}
		else
		{
			var button: FlxUIButton = cast _ui.getAsset(target);
			
			if (button != null)
			{
					trace("it's a button");
				button.setLabelFormat(fontName, button.getLabel().size);
				trace("TextSize:" + button.getLabel().size);
			}
			else
			{
				
				var checkbox:FlxUICheckBox = cast _ui.getAsset(target);
				
				if (checkbox != null)
				{
					trace("it's a checkbox");
					checkbox.getLabel().textField.defaultTextFormat.font =  fontName;
					checkbox.getLabel().textField.defaultTextFormat.size = checkbox.getLabel().size;
					trace("TextSize:" + checkbox.getLabel().size);
				}
				else
				{
					
				}
			
			}
			
		}
		
	}
	
	public override function getEvent(name:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void
	{
	//	trace("getEvent:" + name + " sender:" + sender + " data:" + data + " params:" + params);
		
		switch (name)
		{
			case "finish_load":
				/*initFont("gametitle", "verdana");
				initFont("start", "verdana");
				initFont("popup", "verdana");
				*/
				var text:FlxUIText = cast _ui.getAsset("gametitle");
				if(text!=null)
				formatText(text);

			case "click_button":
				if (params != null && params.length > 0)
				{
					switch (Std.string(params[0]))
					{
						case "facebook": openfl.Lib.getURL(new URLRequest("https://www.facebook.com/Gaia-Island-1367865453318423/"));
						case "language_setting": openSubState(new LanguageSettingPopupState());
						case "start_game": FlxG.switchState(new LevelSelectorState());
					}
				}
		}
		




	}
	
	private function reloadState():Void
	{
		FlxG.switchState(new TitleState());
	}

	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
	}

}