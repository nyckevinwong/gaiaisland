package screenstates;

import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;

/**
 * ...
 * @author Kevin Wong
 */
class PauseState extends FlxState
{
	private var background:FlxSprite;
    private var txtGameScore:FlxText;
	
	public function new() 
	{
		super();
		
		background = new FlxSprite();
		background.makeGraphic(FlxG.stage.stageWidth, FlxG.stage.stageHeight, FlxColor.BLACK);
		background.alpha = 0.5;
		background.scrollFactor.set(0, 0);
		add(background);
		
		txtGameScore = new FlxText(0, 0, 200, "PAUSE", 40);
		txtGameScore.color = FlxColor.GREEN;
		txtGameScore.setBorderStyle(FlxTextBorderStyle.SHADOW, FlxColor.BLACK);
		txtGameScore.setPosition(100,100);
		txtGameScore.scrollFactor.set(0, 0);
		
		add(txtGameScore);
	}
	
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
}