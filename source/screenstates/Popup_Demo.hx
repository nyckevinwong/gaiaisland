package screenstates;
import flixel.addons.ui.FlxUIPopup;
import flixel.FlxG;
import flixel.addons.ui.FlxUIState;
import flixel.addons.ui.FlxUIRadioGroup;
import flixel.addons.ui.FlxUICheckBox;
import utils.FireTongueEx;

class Popup_Demo extends FlxUIPopup
{
	public override function create():Void
	{		
		_xml_id = "popup_demo";
		super.create();
		_ui.setMode("demo_0");
	}
	
		private function hideLocaleLastRadioButtonHack  (radioGroup:FlxUIRadioGroup)
	{
		// locale display has an display issue on the last radio box of a radio group.
		// the last radio text is always hidden or the font is different.
		// this fix just to hide the additional, unused radiobox.
		
					var radios: Array<FlxUICheckBox> = radioGroup.getRadios();
					var radio:FlxUICheckBox;
					var count:Int = 0;
					for (radio in radios)
					{
						count++;
						
						if (count == radios.length)
						{
							radio.visible = false;
						}
					}
	}
	
	public override function getEvent(name:String, target:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void 
	{
		switch(name)
		{
			case "finish_load":
			var radioGroup:FlxUIRadioGroup = cast _ui.getAsset("locale_radio");
			if (radioGroup != null && Main.tongue != null)
			{
				radioGroup.selectedId = Main.tongue.locale.toLowerCase();
				hideLocaleLastRadioButtonHack(radioGroup);
				
			}
			
			this._ui.removeAsset("none");
				
			case "click_radio_group":
			var id:String = cast data;
			if (Main.tongue != null)
			{
				Main.tongue.init(id, reloadState);
			}

			case "click_button":
				
			if (params != null && params.length > 0)
			{
					var i:Int = cast params[0];
					if (_ui.currMode == "demo_0")
					{
						switch (i)
						{
							case 0: openSubState(new Popup_Simple());
							case 1: _ui.setMode("demo_1");
							case 2: close();
						}
					}
					else if (_ui.currMode == "demo_1")
					{
						switch (i)
						{
							case 0: _ui.setMode("demo_0");
							case 1: close();
						}
					}
			}

		}
		
		
		
	}
	
	private function reloadState():Void
	{
		var newState: TitleState = new TitleState();
		FlxG.switchState(newState);
		
		//back to this popup screen
		newState.getEvent("click_button", null, null, ["popup"]);
	}

}