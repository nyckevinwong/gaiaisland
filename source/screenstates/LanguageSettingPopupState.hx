package screenstates;
import flixel.addons.ui.FlxUIPopup;
import flixel.FlxG;
import flixel.addons.ui.FlxUIState;
import utils.FireTongueEx;
import flash.text.TextField;
import flixel.addons.ui.FlxUIButton;
import flixel.addons.ui.FlxUISprite;
import flixel.addons.ui.FlxUIRadioGroup;
import flixel.addons.ui.FlxUICheckBox;
import flixel.addons.ui.FlxUIText;

class LanguageSettingPopupState extends FlxUIPopup
{
	public override function create():Void
	{		
		_xml_id = "languagesetting_state";
		
		super.create();
	}
	
	private function initFont(targetName:String, defaultFontName:String, groupName:String=null)
	{
		var text:FlxUIText = cast _ui.getAsset(targetName);
		var fontName: String = Main.tongue.getFont(defaultFontName);
		
		trace("target:" + targetName + " -> initFont:" +fontName);
		
		if (text != null)
		{
			trace("it's a text box");
			text.setFormat(fontName, text.size);
			trace("TextSize:" + text.size);
		}
		else
		{
			var button: FlxUIButton = cast _ui.getAsset(targetName);
			
			if (button != null)
			{
					trace("it's a button");
				button.setLabelFormat(fontName, button.getLabel().size);
				trace("TextSize:" + button.getLabel().size);
			}
			else
			{
				
				var checkbox:FlxUICheckBox = cast _ui.getAsset(targetName);
				
				if (checkbox != null)
				{
					trace("it's a checkbox");
					trace("default font: " + checkbox.getLabel().textField.defaultTextFormat.font);
					checkbox.getLabel().textField.defaultTextFormat.font =  fontName;
					checkbox.getLabel().textField.defaultTextFormat.size = checkbox.getLabel().size;
					trace("TextSize:" + checkbox.getLabel().size);
				}
				else
				{
					var radioBox:FlxUICheckBox = getRadioBox(groupName, targetName);
					if (radioBox != null)
					{
					var label: FlxUIText = radioBox.getLabel();
					trace("it's a radioBox: " + label.textField.text);
					trace("default font: " + label.textField.defaultTextFormat.font);
					label.textField.defaultTextFormat.font =  fontName;
					label.textField.defaultTextFormat.size = radioBox.getLabel().size;
					trace("TextSize:" + radioBox.getLabel().size);
					}
				}
			
			}
			
		}
		
	}
	
	private function hideLocaleLastRadioButtonHack  (radioGroup:FlxUIRadioGroup)
	{
		// locale display has an display issue on the last radio box of a radio group.
		// the last radio text is always hidden or the font is different.
		// this fix just to hide the additional, unused radiobox.
		
					var radios: Array<FlxUICheckBox> = radioGroup.getRadios();
					var radio:FlxUICheckBox;
					var count:Int = 0;
					for (radio in radios)
					{
						count++;
						
						if (count == radios.length)
						{
							radio.visible = false;
						}
					}
	}

	private function getRadioBox(radioGroupName:String, indexPos:String): FlxUICheckBox
	{
					var radioGroup:FlxUIRadioGroup = cast _ui.getAsset(radioGroupName);
					var index:Int = Std.parseInt(indexPos);

					if (radioGroup != null)
					{
						var radios: Array<FlxUICheckBox> = radioGroup.getRadios();
						var radio:FlxUICheckBox;
						var count:Int = 0;
						for (radio in radios)
						{
							count++;
							if(count==index)
							return radio;
						}
					}
					return null;
	}

	public override function getEvent(name:String, target:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void 
	{
			trace("getEvent:" + name + " sender:" + target + " data:" + data + " params:" + params);
			
		switch(name)
		{
			case "finish_load":
			var radioGroup:FlxUIRadioGroup = cast _ui.getAsset("locale_radio");
			if (radioGroup != null && Main.tongue != null)
			{
				radioGroup.selectedId = Main.tongue.locale.toLowerCase();
				hideLocaleLastRadioButtonHack(radioGroup);				
			}
		/*		
			if (params != null && params.length > 0)
			{
				trace("parameters:" + params[0] + "," +params[1]);
			}
			*/
	
			initFont("2", "DroidSans","locale_radio");
			initFont("closeBtn", "DroidSans");
		
			case "click_radio_group":
			var id:String = cast data;
			if (Main.tongue != null)
			{
				Main.tongue.init(id, reloadState);
			}
			
			case "click_button":
				
			if (params != null && params.length > 0)
			{
					var action:Int = cast params[0];
					
					switch(action)
					{
						case 2: close();
					}
			}
		}
		
		
		
	}
	
	private function reloadState():Void
	{
		var newState: TitleState = new TitleState();
		FlxG.switchState(newState);
		
		//back to this popup screen
		newState.getEvent("click_button", null, null, ["language_setting"]);
	}

}