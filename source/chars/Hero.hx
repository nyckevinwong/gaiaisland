package chars;

import chars.Character;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.util.FlxColor;
import openfl.display.Tile;
import states.Conditions;
import spriteutils.SpriteAnim;

import flixel.addons.editors.tiled.TiledTile;

class Hero extends Character
{	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
	
		loadGraphic(AssetPaths.john_side__png, true, 16, 24);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		facing = FlxObject.RIGHT;
		
		animation.add("standing", [1], 3);
		animation.add("walking", [0, 1,3], 12);
		animation.add("jumping", [2]);
		animation.add("pound", [1]);
		animation.add("landing", [0, 1, 0, 1], 8, false);
		animation.add("grabing-from-down", [4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6],12, false);
		
		this.offset.x = 3;
		this.width = 10;
		
		fsm = new FlxFSM<FlxSprite>(this);
			fsm.transitions
			.add(Idle, Jump, Conditions.jump)
			.add(Idle, Grabbing, Conditions.grab)
			.add(Idle, EnterPortalVase, Conditions.enterPortalVase)
			.add(Idle, Climb, Conditions.climb)
			.add(Climb, Idle, Conditions.offLadder)
			.add(Climb, Idle, Conditions.grounded)
			.add(Climb, Jump, Conditions.climbJump)
			.add(Grabbing, Idle, Conditions.animationFinished)
			.add(Jump, Idle, Conditions.grounded)
			.add(Jump, Climb, Conditions.climb)
			.add(SpringJump, Idle, Conditions.grounded)
			.add(SpringJump, Climb, Conditions.climb)
//			.add(Jump, GroundPound, Conditions.groundSlam)
			.add(GroundPound, GroundPoundFinish, Conditions.grounded)
			.add(GroundPoundFinish, Idle, Conditions.animationFinished)
			.start(Idle);
			
	}
	
	// hit by hero head
	private override function onHitTilesOnTop(tileX:Int, tileY:Int)
	{
		var tileIndex: Int = GameObject.map.getTile(tileX, tileY);
		
		
		switch(tileIndex)
		{
			case 202:
			GameObject.map.setTile(tileX, tileY, 0, true);

			if (this._blockgibs != null)
			{
				this._blockgibs.setPosition(tileX * 16, tileY * 16);
				this._blockgibs.start(true, 2.8);
				FlxG.sound.play(AssetPaths.breakbrick__ogg, 1, false);
			}
			
			case 241:
			GameObject.map.setTile(tileX, tileY, 205, true);
			generateItem(tileX, tileY - 1, Item.COIN); 			
			
			case 244:
			GameObject.map.setTile(tileX, tileY, 205, true);
			generateItem(tileX, tileY - 1, Item.APPLE); 
			
			case 205:
			FlxG.sound.play(AssetPaths.hitsolid__ogg, 1, false);
			default:
		}
	}
	
	private function generateItem(tileX:Float, tileY:Float, itemType:Int, hitHead:Bool= true)
	{
			if (Character.addItemCallBack != null)
			{
				var vx: Float = (this.facing == FlxObject.RIGHT) ? -90:90;
				var item : Item = new Item(tileX * 16, tileY  * 16, itemType);
				item.spawner = true;
				item.startVelocity = new FlxPoint(vx, 0); 				
				
				if(hitHead==false)
				item.setSpriteAnimDirection(SpriteAnim.UPAPPEAR);
				
				Character.addItemCallBack(item);				
			}		
	}
	
	private override function onHitTilesAtBottom(tileX:Int, tileY:Int)
	{
		var tileIndex: Int = GameObject.map.getTile(tileX, tileY);
		
		switch(tileIndex)
		{
			case 202:
			GameObject.map.setTile(tileX, tileY, 0, true);

			if (this._blockgibs != null)
			{
				this._blockgibs.setPosition(tileX * 16, tileY * 16);
				this._blockgibs.start(true, 2.8);
				FlxG.sound.play(AssetPaths.breakbrick__ogg, 1, false);
			}
			
			case 241:
			GameObject.map.setTile(tileX, tileY, 205, true);
			generateItem(tileX, tileY + 1, Item.COIN, false); 
				
			case 244:
			GameObject.map.setTile(tileX, tileY, 205, true);
			generateItem(tileX, tileY + 1, Item.APPLE, false); 
			
			case 205:
			FlxG.sound.play(AssetPaths.hitsolid__ogg, 1, false);
			default:
		}
	}
	
	
	
	
}

