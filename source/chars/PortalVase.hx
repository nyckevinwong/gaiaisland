package chars;

import flixel.FlxSprite;
import states.Conditions;
/**
 * ...
 * @author ...
 */
class PortalVase  extends Portal
{
	private var elapsedTotal: Float = 0;
	private var state:String = "done";
	private var enterY:Float = 0;
	private var exitY:Float = 0;
	
	public function new(portalID:Int, X:Float = 0, Y:Float = 0)
	{
		super(X, Y , portalID);	

		animation.add("idle", [4], 12, true);		
		animation.play("idle");
		immovable = true;
	}
	
	public override function isTransferingComplete() : Bool
	{
		return (state == "done");
	}
	
	public override function update(elapsed:Float):Void 
	{
		switch(state)
		{
		case "entering":
			// adjust to the current X position of portal while entering
			this.portalTravller.x = this.x;

			if (this.portalTravller.y < enterY)
			{
				this.portalTravller.y += this.portalTravller.height * elapsed;
			}
			else
			{
				state = "transfering";
			}
			
		case "transfering":
			if (this.targetExitPortal != null)
			{
				var portalX : Float = this.targetExitPortal.x;
				var portalY : Float = this.targetExitPortal.y;

				this.portalTravller.x = portalX;
				this.portalTravller.y = portalY;
				state = "exiting";			
				this.notifyObservers();
			}
		case "exiting":
			if (this.targetExitPortal != null)
			{

				if (this.portalTravller.y > exitY)
				{
					this.portalTravller.y -=this.portalTravller.height * elapsed;	
				}
				else
				{
					state = "finalize";			
				}
			}
		case "finalize":
			this.portalTravller.immovable = false;
			this.portalTravller.solid = true;
			state = "done";
		}	
			
		
		super.update(elapsed);
	}
	
	
	public override function enterPortal(traveller:Character)
	{
		super.enterPortal(traveller); // call parent function to set up portalTravller and clear observables

		elapsedTotal = 0;
		state = "entering";
		
		this.portalTravller.immovable = true;
		this.portalTravller.solid = false;

		enterY = this.portalTravller.y + this.portalTravller.height;
		exitY = this.targetExitPortal.y - this.portalTravller.height + 1;
	}
	
	
}