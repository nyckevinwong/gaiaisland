package chars;
import flixel.FlxSprite;
import flixel.FlxObject;

/**
 * ...
 * @author Kevin Wong
 */
class FallingBrick extends GameObject
{
	var touchElapsed: Float = 0;
	
	public function new(?X:Float = 0, ?Y:Float = 0)
	{
		super(X, Y);
		loadGraphic(AssetPaths.tiles__png, true, 16, 16);
		animation.add("idle", [220], 1); // tileId -1 = actual position on tile graphic
		animation.add("touched", [220, 300], 16); // use 300 as a empty tile
		animation.play("idle");
		this.immovable = true;	
		this.allowCollisions = FlxObject.UP;
	}
	
	
	// no reset to initial position
	public override function resetInitPosition()
	{
		
	}
	
	public override function update(elapsed:Float):Void 
	{		

		if (touchElapsed <= 1 )
		{
			if (this.touching == FlxObject.UP)
			{
				// flash the tiles to indicate it's going to fall when touched
				if (animation.finished || animation.name == "idle")
					animation.play("touched");
					
				touchElapsed += elapsed;
			}
			else
			{
				//  when not touched
					animation.play("idle");
			}
			
		}
		else if (touchElapsed > 1) // if the total touch is more than 1 second, it will start to fall.
		{
			if (animation.name == "touched")
				animation.play("idle");
				
			// use lowr gravity than hero's. this can make hero jump works due to hero can stand on it after each super.update() call.
			this.acceleration.y = GameObject.GRAVITY-10; 
		}
		
		super.update(elapsed);
	}
	
}