package chars;

import flixel.FlxSprite;
import flixel.math.FlxPoint;

/**
 * ...
 * @author Kevin Wong
 */
class Elevator extends FlxSprite
{
	
	public function new(?X:Float = 0, ?Y:Float = 0)
	{
		super(X, Y);
		this.immovable = true;	
		loadGraphic(AssetPaths.elevator__png);
	}
	
	
}