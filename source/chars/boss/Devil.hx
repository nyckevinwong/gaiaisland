package chars.boss;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import states.Conditions;
import flixel.math.FlxRect;
import maputils.Rect;

/**
 * ...
 * @author ...
 */
class Devil extends GameObject
{
	public var isDead :Bool = false;
	private var diyingMode :Bool = false;
	private var elapsedTotal: Float = 0;
	public var bossArea:Rect=null;
	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
		loadGraphic(AssetPaths.red_devil__png, true, 64, 44);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		
		
		animation.add("fly-idle", [0, 3], 3);
		animation.add("fly-attack", [0, 1 , 2, 3], 12);
		animation.add("fly-up-back", [4, 5 , 6, 7], 12);
		animation.add("walking", [8,9,10,11]);
		
		facing = FlxObject.RIGHT;
		velocity.x = 0;
		velocity.y = 0;
		acceleration.y = GameObject.GRAVITY;
		acceleration.x = -40;
		solid = true; // collide with hero to damage hero
		animation.play("fly-idle");	
		health = 6;
		diyingMode = false;
		isDead = false;
	}
	


	override public function update(elapsed:Float):Void 
	{		

		if (bossArea.overlaps(GameObject.activeArea)) // this check is better than instance checking . sometimes instance checking apporach causes an issue.
		{// when play enter this activeArea
			
			elapsedTotal += elapsed;
			
			if (!diyingMode)
			{
				
				if (this.health <= 3)
				{
					if (velocity.x > 0)
					acceleration.x = 65;
					else
					acceleration.x = -65;
				}
				
				if (elapsedTotal  > 0.15)
				{
					velocity.y = -35;
					elapsedTotal = 0;
				}
				
				
				if (this.x <= bossArea.left)
				{
					this.x = bossArea.left;
					velocity.x = 30;
					acceleration.x = 40;
				}
				
				// if it hits the end of the world, go opposite.
				if (this.x + this.width > bossArea.right)
				{
					this.x = bossArea.right - this.width;
					velocity.x = -30;
					acceleration.x = -40;
				}
				
				if (this.y < bossArea.top)
				{
					this.y = bossArea.top;
					this.velocity.y = 280;
				}
				
				if (this.y+this.height+16 > bossArea.bottom)
				{
					this.y = bossArea.bottom - this.height+16;
					this.velocity.y = -280;
				}
		
			}
			else if(!isDead)
			{ // play dead animation
										
				this.flipY = true;
				this.velocity.y = 10;
				this.color = 0x00ffffff;				
				solid = false; // can't damage player once it's dead.
				
				if (elapsedTotal >= 2.00)
				{
					isDead = true;
					FlxG.sound.play(AssetPaths.largemonsterdeath__ogg, 1, false);
					kill();
					elapsedTotal = 0;
					bossArea = null;
				}
				
			}
			
			super.update(elapsed);
		}				
	}
	
	override public function kill()
	{
		if (this.health <= 0 && !isDead )
		{
			// it was killed by a player.. it is diying.
			diyingMode = true;
			elapsedTotal = 0;
		}
		else
		{
			super.kill(); // normal kill
		}
	}
	
}