package chars;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;

import flixel.tile.FlxTilemap;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import chars.TemporaryInfo;

import flixel.effects.particles.FlxEmitter;

class Character extends GameObject
{
	public static inline var BULLET_SPEED:Int = 200;
	public static inline var GUN_DELAY:Float = 0.4;
	public static inline var GRAVITY: Float = 600;
	public static var addItemCallBack: Item->Void= null;
	public var tempInfo: TemporaryInfo;

	public var fsm:FlxFSM<FlxSprite>;
	public var rideOnTarget: FlxSprite;
	public var grabbingTarget: FlxSprite;
	public var holdingTarget: FlxSprite;
	
	private var _blt:Bullet;
	private var _cooldown:Float;
	private var _bullets:FlxTypedGroup<Bullet>;
	public var _onPortal:Portal = null;

	public var maxHealth:Float = 3;
	public var hurtElapsed:Float = 0;
	private var _gibs:FlxEmitter;
	public var _blockgibs:FlxEmitter;
	
	public var climbing:Bool = false;
	
	public function setDeathEmitter(gibs:FlxEmitter)
	{
		this._gibs = gibs;
	}
	
	public function setBlockEmitter(gibs:FlxEmitter)
	{
		this._blockgibs = gibs;
	}
	
	public function updateHealth(hp:Float)
	{
		if (maxHealth <= hp)
		this.health = maxHealth;
		else
		this.health = hp;
	}
	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
		_cooldown = GUN_DELAY;
	}
	
	public  function setBulletPool(bullets:FlxTypedGroup<Bullet>)
	{
		this._bullets = bullets;
	}
	
	private override function onMapBoundXCheck()
	{
			// if it's out of map boundary on horizontal side	
			if (this.x < GameObject.activeArea.left)
			{
				this.x = GameObject.activeArea.left;
			}
			else if (this.x+this.width > GameObject.activeArea.right)
			{
				this.x = GameObject.activeArea.right-this.width;				
			}	
	}
		
	private override function onMapBoundYCheck()
	{		
			// if it's out of map boundary on vertical side
			
			if (this.y-this.height < 0) // jump/fly outside the map
			{
				if (this.velocity.y < 0 && this.acceleration.y < 0 )
				{ // the object is flying outside of the screen

				}
			}
			else if (this.y > GameObject.activeArea.bottom)
			{
				// fall/jump off from the platfrom/mountain/etc into the death
				super.hurt(100);	
				super.kill();
			}
			
	}
	
	override public function update(elapsed:Float):Void 
	{		
		if (fsm != null) fsm.update(elapsed);
	
		_cooldown += elapsed;
		hurtElapsed += elapsed;
		
		if (hurtElapsed >= 2.5)
		{
			this.alpha = 1; // BACK TO NORMAL
		}
		
		super.update(elapsed);
		
		
		// moved performHolding after update can fix the issue where the holding item is floating up and down with the character.		
		this.performHolding();
	}
	
	public override function destroy():Void 
	{
		if(fsm != null) fsm.destroy();
		fsm = null;
		
		super.destroy();
	}
	
	public override function hurt(Damage:Float):Void
	{
		if (hurtElapsed >= 2.5) // hurt can not be called within 2.5 second
		{
			super.hurt(Damage); 
			hurtElapsed = 0;
			this.alpha = 0.5;
		}
		
	}
	
	public override function kill():Void
	{
		if (!alive) 
		{
			return;
		}
		
		super.kill();

		FlxG.cameras.shake(0.005, 0.35);
		FlxG.camera.flash(FlxColor.WHITE, 0.35);
		
		if (_gibs != null)
		{
			_gibs.focusOn(this);
			_gibs.start(true, 2.80);
		}
	}
		
	public function ride(rideOnTarget: FlxSprite)
	{
		this.rideOnTarget = rideOnTarget;
	}
	
	public function perfromRide()
	{
		if (this.rideOnTarget != null)
		{
		    this.velocity.x = this.rideOnTarget.velocity.x;
		}
	}
	
	public function grabTarget()
	{
		if (this.rideOnTarget != null)
		{
			this.grabbingTarget = this.rideOnTarget;
			this.rideOnTarget = null; // not riding on this anymore
		}
	}
	
	public function isGrabbing(): Bool
	{
		return this.grabbingTarget != null;
	}
	
	// currently on something grabble?
	public function onGrabble(): Bool
	{
		return false;
	}
	
	public function isHolding(): Bool
	{
		return this.holdingTarget != null;
	}
	
	// use this function in fsm state only
	public function performGrabbing()
	{
		   if (isGrabbing())
		   {
				// holdingTarget is actualy grabingTarget
				this.grabbingTarget.acceleration.x = 0;
				this.grabbingTarget.acceleration.y = 0;
				this.grabbingTarget.velocity.x = 0;
				this.grabbingTarget.velocity.y = 0;
				this.grabbingTarget.x = this.x;

				this.grabbingTarget.allowCollisions = FlxObject.NONE;
				this.grabbingTarget.y -= 3;

				if (this.grabbingTarget.y < (this.y - this.grabbingTarget.height) )
				{
					this.grabbingTarget.y = this.y - this.grabbingTarget.height;
					// not grabbing target anymore
					this.holdingTarget = this.grabbingTarget;					
					this.grabbingTarget = null;
				}
				
		   }

	}
	
	public function isHoldingItem(itemID:Int):Bool
	{
		if (Std.is(this.holdingTarget, Item))
		{
			var item : Item = cast(this.holdingTarget, Item);
			
			if (item.itemID == itemID )
				return true;
		}
		
		return false;
	}
	
	public function performHolding()
	{
		if (this.holdingTarget != null)
		{		
			
			if (Std.is(this.holdingTarget, Item))
			{
				this.holdingTarget.facing = this.facing;
				this.holdingTarget.y = this.y + (this.height / 2);
				this.holdingTarget.flipX = (this.facing == FlxObject.LEFT);
				
				if (this.facing == FlxObject.RIGHT)
				{
					this.holdingTarget.x = this.x + this.offset.x + this.width - 8;
				}
				else
				{
					this.holdingTarget.x = this.x - this.offset.x - this.width;
				}
				
			//	var ItemID: 
			}
			else
			{
				this.holdingTarget.x = this.x;
				this.holdingTarget.y = this.y - this.holdingTarget.height;		
			}
		}
		
	}
	
	public function throwObject()
	{
		// throw object should be a type of weapon where holding object is removed from the scene and throw a new one 
		
		if (this.holdingTarget != null)
		{
			var sprite : FlxSprite = this.holdingTarget;
			this.holdingTarget = null;
			
			var dir : Int = (this.facing == FlxObject.RIGHT) ? 1: -1;
			
			sprite.x = this.x + dir * (sprite.width + 10);
			sprite.velocity.x = dir *100;
			sprite.velocity.y = 100;
			sprite.allowCollisions = FlxObject.ANY;
		}
		
	}
		
	public function fireWeapon()
	{
		if (this.isHolding())
		{
			shoot();
		}

	}
	
	public function show(visible:Bool)
	{
		this.visible = visible;
		
		if (this.holdingTarget != null)
			this.holdingTarget.visible = visible;
	}
	
	public function enterPortal()
	{
		if (this._onPortal != null)
		{
			this._onPortal.enterPortal(this);
		}
	}
	
	private function shoot():Void 
	{
		if (isHoldingItem(Item.SHOTGUN))
		{
			var startX: Float = this.x;
			
			if (this.facing == FlxObject.LEFT)
			{
				startX -= this.width;
			}
			else
			{
				startX += this.width;
			}

			if (_bullets != null)
			{
				// Prepare some variables to pass on to the bullet
				var bulletX:Int = Math.floor(startX);
				var bulletY:Int = Math.floor(y + (this.height/2));
				var bXVeloc:Int = 0;
				var bYVeloc:Int = 0;
				
				if (_cooldown >= GUN_DELAY)
				{
					_blt = _bullets.recycle(Bullet.new);
					
					if (_blt != null)
					{
						if (flipX)
						{
							// nudge it a little to the side so it doesn't emerge from the middle of helmutguy
							bulletX -= Math.floor(_blt.width - 8); 
							bXVeloc = -BULLET_SPEED;
						}
						else
						{
							bulletX += Math.floor(width - 8);
							bXVeloc = BULLET_SPEED;
						}
						
						_blt.shoot(bulletX, bulletY, bXVeloc, bYVeloc);
						FlxG.sound.play(AssetPaths.shoot__ogg, 1, false);
						// reset the shot clock
						_cooldown = 0; 
					}
				}
			}
		
		}
		else
		{
			throwObject();
		}
	}
	
}
