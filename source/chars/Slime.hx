package chars;


import chars.Character;
import chars.GameObject;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import states.Conditions;

class Slime extends Character
{	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
		loadGraphic(AssetPaths.slime__png, true, 16, 16);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		facing = FlxObject.RIGHT;
		
		animation.add("standing", [0, 1], 3);
		animation.add("walking", [0, 1], 12);
		animation.add("jumping", [2]);
		animation.add("pound", [3]);
		animation.add("landing", [4, 0, 1, 0], 8, false);
		
		acceleration.y = GameObject.GRAVITY;
		maxVelocity.set(100, GameObject.GRAVITY);
		
		
		fsm = new FlxFSM<FlxSprite>(this);
			fsm.transitions
			.add(Idle, Jump, states.Conditions.jump)
			.add(Jump, Idle, states.Conditions.grounded)
			.add(Jump, DoubleJump, states.Conditions.airJump)
			.add(DoubleJump, TripleJump, states.Conditions.airJump)
			.add(DoubleJump, Idle, states.Conditions.grounded)
			.add(TripleJump, Idle, states.Conditions.grounded)
			.add(Jump, GroundPound, states.Conditions.groundSlam)
			.add(GroundPound, GroundPoundFinish, states.Conditions.grounded)
			.add(GroundPoundFinish, Idle, states.Conditions.animationFinished)
			.start(Idle);
			
	}
}



