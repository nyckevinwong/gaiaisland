package chars;

import flixel.text.FlxText;

import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class TemporaryInfo extends FlxText
{

	public function new() 
	{
		super();
		this.color = FlxColor.WHITE;
		this.setBorderStyle(SHADOW, FlxColor.RED);
		this.alignment = CENTER;
		this.visible = false;
	}
	
	public function displayInfo()
	{
		this.alpha = 0;
		this.visible = true;
		// setup 2 tweens to allow the damage indicators to fade in and float up from the sprites
				FlxTween.num(this.y, this.y-12, 1, { ease:FlxEase.circOut}, updateDisplayPosition);
				FlxTween.num(0, 1, .2, { ease: FlxEase.circInOut, onComplete: OnCompleteFadingIn }, updateDisplayAlpha);
	}
	
	/**
	 * This function is called from our Tweens to move the damage displays up on the screen
	 * @param	Value
	 */
	private function updateDisplayPosition(Y:Float):Void
	{
		this.y = Y;
	}
	
	/**
	 * This function is called from our Tweens to fade in/out the damage text
	 */
	private function updateDisplayAlpha(Alpha:Float):Void
	{
		this.alpha = Alpha;
	}
	
	/**
	 * This function is called when our damage texts have finished fading in - it will trigger them to start fading out again, after a short delay
	 */
	private function OnCompleteFadingIn(_):Void
	{
		FlxTween.num(1, 0, .66, { ease: FlxEase.circInOut, startDelay: 1, onComplete: OnCompleteFadingOut}, updateDisplayAlpha);
	}
	
	private function OnCompleteFadingOut(_):Void
	{
		this.visible = false;
	}
	
}