package chars;

import flixel.FlxSprite;
import designpatterns.observer.Observable;
import designpatterns.observer.Observer;

/**
 * ...
 * @author ...
 */
class Portal extends FlxSprite
{
	private static var portalMap:Map<Int, Portal> = new Map<Int, Portal>(); 	
	private var targetExitPortal:Portal;
	private var portalTravller: Character;
	private static var observable: Observable<Character> = new Observable<Character>();
	public var disappearAfterEnter: Bool = false;
		
	public function new(X:Float = 0, Y:Float = 0, portalID:Int)
	{
		super(X, Y);	
		loadGraphic(AssetPaths.doors__png, true, 16, 32);
		
		if (portalMap.exists(portalID))
		{
			var foundPortal:Portal = portalMap.get(portalID);

			if (foundPortal != this) // foundPortal has the same portal ID
			{
				// portal ID represents these two portals are connected each other. 
				foundPortal.setExitPortal(this);
				this.setExitPortal(foundPortal);
			}
			
			portalMap.remove(portalID); // finish setup, remove this portal ID.
		}
		else
		{
			portalMap.set(portalID, this);
		}
		
	}
	
	public function isTransferingComplete() : Bool
	{
		return true;
	}
	
	private function setExitPortal(exit:Portal)
	{
		this.targetExitPortal = exit;
	}
	
	public function setPortalTravller(traveller: Character)
	{
		this.portalTravller = traveller;
	}
		
	public function enterPortal(traveller:Character)
	{
		this.portalTravller = traveller;
		Portal.observable.clearChanged();
	}
	
	public static function addObserver(observer:Observer<Character>) : Bool
	{
		// only one observer is allowed
		if (observable.countObservers() > 1)
		return false;
	
		return observable.addObserver(observer);
	}
		
	public function notifyObservers()
	{
		Portal.observable.setChanged();
		Portal.observable.notifyObservers(this.portalTravller);
	}
	
	public static function clearOversables()
	{
		Portal.observable.removeObservers();
	}
}