package chars;

import chars.Character;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import states.Conditions;

class PatrolGuy extends Character
{
	
	public function new(X:Float = 0, Y:Float = 0, enemyType:Int=0)
	{
		super(X, Y);
		
		loadGraphic(AssetPaths.enemy_partol__png, true, 16, 11);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		facing = FlxObject.RIGHT;
		
		var startIndex: Int = 0;
		
		switch(enemyType)
		{
		case 0:
			health  = 2;
			ACCELERATION_X = 10;
		case 1:
			health  = 3;
			ACCELERATION_X = 20;
			startIndex = 3;
		case 2:
			health = 5;
			ACCELERATION_X = 30;
			startIndex = 6;
		}

		animation.add("standing", [0+startIndex], 3);
		animation.add("walking", [1+startIndex,2+startIndex], 12);
		animation.add("jumping", [0+startIndex,1+startIndex,2+startIndex]);
		animation.add("landing", [0+startIndex], 3);
				

		fsm = new FlxFSM<FlxSprite>(this);
			fsm.transitions
			.add(Idle, Jump, states.Conditions.jump)
			.add(Idle, EnterPortalVase, Conditions.enterPortalVase)
			.add(Jump, Idle, states.Conditions.grounded)
			.start(Idle);		
	}
	
}

