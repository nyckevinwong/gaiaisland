package chars;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import states.Conditions;

/**
 * ...
 * @author ...
 */
class PatrolEnemy extends GameObject
{		

	private var enableHitEmptyTileAtBottomBehavior:Bool = true;
	public function new(X:Float = 0, Y:Float = 0, enemyType:Int=0)
	{
		super(X, Y);
		
		loadGraphic(AssetPaths.enemy_partol__png, true, 16, 11);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		
		var startIndex: Int = 0;
		
		switch(enemyType)
		{
		case 0:
			health  = 2;
			ACCELERATION_X = 10;
		case 1:
			enableHitEmptyTileAtBottomBehavior = false;
			health  = 3;
			ACCELERATION_X = 20;
			startIndex = 3;
		case 2:
			health = 5;
			ACCELERATION_X = 30;
			startIndex = 6;
		}
			
		
		animation.add("standing", [0+startIndex], 3);
		animation.add("walking", [1+startIndex,2+startIndex], 12);
		animation.add("jumping", [0+startIndex,1+startIndex,2+startIndex]);
		
		facing = FlxObject.RIGHT;
		acceleration.y = GameObject.GRAVITY;
		acceleration.x = ACCELERATION_X;
		maxVelocity.set(100, GameObject.GRAVITY);	
		isGrabble = true;
		isRiddible = true;
	
		
		animation.play("walking");		
	}
	
	public override function reset(X:Float, Y:Float)
	{
		super.reset(X, Y);

		facing = FlxObject.RIGHT;
		acceleration.x = ACCELERATION_X;
		acceleration.y = GameObject.GRAVITY;
		maxVelocity.set(100, GameObject.GRAVITY);			
	}
	
	private override function onMapBoundYCheck()
	{		
			// if it's out of map boundary on vertical side
			
			if (this.y-this.height < 0) // jump/fly outside the map
			{
					this.kill(); 
					this.resetInitPosition();
			}
			else if (this.y >= (GameObject.map.height - this.height*2))
			{
				this.y = GameObject.map.height - this.height * 2;
				
				// fall/jump off from the platfrom/mountain/etc into the death
				this.kill();				
				this.resetInitPosition();
			}
			
	}
	
	
	
	public override function update(elapsed:Float):Void 
	{		
		updateFacing();
		
		if (GameObject.map != null)
		{
			
			var x1: Int = Std.int(this.x / 16);
			var y1: Int = Std.int(this.y / 16); 
			
			changeDirectionWhenHitWall(x1, y1);
			
			if(enableHitEmptyTileAtBottomBehavior)
			changeDirectionWhenHitEmptyTileAtBottom(x1, y1);
			
		}
		
		
		super.update(elapsed);
	}

}