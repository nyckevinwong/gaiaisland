package chars;

import flash.display.Sprite;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.addons.util.FlxFSM.FlxFSMState;
import flixel.math.FlxRect;
import states.Conditions.SpringJump;
import states.Conditions.SuperJump;
import states.Conditions.Jump;
import flixel.FlxG;

/**
 * ...
 * @author Kevin Wong
 */
class Spring extends FlxSprite
{

	private var originalY:Float;
	public var hero:Character;
	private var lastBounceFrame:Bool = false;
	
	public function new(X:Float = 0, Y:Float = 0, movable:Bool= false)
	{
		super(X, Y);
		originalY = Y;
		this.immovable = !movable;

		loadGraphic(AssetPaths.objects__png, true, 16, 16);		
		animation.add("idle", [100], 1, false);	
		animation.add("bounce", [100, 101, 102, 101, 100], 48, false);
		
		animation.play("idle");
	}
	
	private function loadAnimationFrame(name:String, frameNums:Array<Int>)
	{
		animation.add(name, frameNums, 1, false);
	}
	
	public override function update(elapsed:Float):Void 
	{		

		switch(animation.frameIndex)
		{
			case 100:
				this.y = originalY;
				this.offset.set();
				this.setSize(16, 16);
			case 101:
				this.y = originalY+3;
				this.offset.set(0,2);
				this.setSize(16, 13);
			case 102:
				this.y = originalY+6;
				this.offset.set(0,5);
				this.setSize(16, 10);
		}


		switch(animation.name)
		{
		case "idle":
			if (animation.finished)
			{
					if (this.isTouching(FlxObject.UP)) // something is on it
					{
						FlxG.sound.play(AssetPaths.spring__ogg, 1, false);
						animation.play("bounce");
					}
			}
			
		case "bounce":
			if (animation.finished)
			{
					var state : FlxFSMState<FlxSprite> = hero.fsm.state;
					if (!Std.is(state,Jump))
					{
						hero.fsm.state = new SpringJump();
					}
				animation.play("idle");
			}
		}
		
	
		super.update(elapsed);
	}
}