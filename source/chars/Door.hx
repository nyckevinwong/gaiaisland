package chars;

import flash.display.Bitmap;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import maputils.MapUtils;
import flixel.FlxObject;

/**
 * ...
 * @author ...
 */
class Door  extends Portal
{
    private var entering:Bool = false;
	
	public function new(portalID:Int, X:Float = 0, Y:Float = 0)
	{
		super(X, Y , portalID);	

		animation.add("idle", [0], 12, false);
		animation.add("open", [0, 1, 2], 12, false);
		animation.add("close", [2, 1, 0], 12, false);
		animation.add("close-when-arrive", [2, 1, 0], 12, false);
		animation.play("idle");
		
		this.acceleration.y =  Character.GRAVITY;
		this.maxVelocity.set(100, Character.GRAVITY);
	//	this.disappearAfterEnter = true;
	}
	
	public override function isTransferingComplete() : Bool
	{
		return animation.curAnim.name == "idle" && entering == false;
	}
		
	private function transferToRight(x:Float, y:Float) : Bool
	{
		return MapUtils.horizontalRayOverlapAnyTile(x, y, 3 * GameObject.map.tileWidth, GameObject.map) == false;
	}
	
	private function transferToLeft(x:Float, y:Float) : Bool
	{
		var totalWidth: Float = 3 * GameObject.map.tileWidth;
		
		return MapUtils.horizontalRayOverlapAnyTile(x -totalWidth, y, totalWidth, GameObject.map) == false;
	}

	public override function update(elapsed:Float):Void 
	{
			if (animation.curAnim.name == "open" && animation.finished == true )
			{
				this.portalTravller.show(false);
				animation.play("close");
			}
			else if (animation.curAnim.name == "close" && animation.finished == true)
			{
				
				if (this.targetExitPortal != null)
				{
					var portalX : Float = this.targetExitPortal.x;
					var portalY : Float = this.targetExitPortal.y +this.targetExitPortal.height;
					portalY -= this.portalTravller.height;
					
					// determine where shall transfer the target player to
					// we only transfer player to the non-blocking tiles.
					if (transferToRight(portalX, portalY))
					{
						portalX += this.targetExitPortal.width + 1;	
						this.portalTravller.facing = FlxObject.RIGHT;
					}
					else if(transferToLeft(portalX, portalY))
					{
						portalX -= this.portalTravller.width + 1;												
						this.portalTravller.facing = FlxObject.LEFT;
					}
					else
					{
						//back to use default . still right
						portalX += this.targetExitPortal.width + 1;	
						this.portalTravller.facing = FlxObject.RIGHT;
						
					}
					
					this.portalTravller.x = portalX;
					this.portalTravller.y = portalY;									
					this.portalTravller.show(true);
					this.portalTravller._onPortal = null;
					this.notifyObservers();
					this.targetExitPortal.animation.play("close-when-arrive");
				}
				
				
				animation.play("idle");
				entering = false;
			}
			else if (animation.curAnim.name == "close-when-arrive" && animation.finished == true)
			{
					if (this.disappearAfterEnter)
					{
						this.visible = false;
						this.kill();
					}

					animation.play("idle");
			}
			
		super.update(elapsed);
	}
	
	
	public override function enterPortal(traveller:Character)
	{
		if (this.isTransferingComplete())
		{
			entering = true;
			super.enterPortal(traveller); // call parent function to set up portalTravller and clear observables
			animation.play("open");
		}
	}
	
	
}