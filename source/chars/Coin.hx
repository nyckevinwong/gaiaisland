package chars;

import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Coin extends FlxSprite
{
	// individual coin floating over the background
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
		
		loadGraphic(AssetPaths.coinspin__png, true, 16, 16);
		animation.add("spinning", [0, 1, 2, 3], 10, true);
		animation.play("spinning");
	}
	
	override public function kill():Void
	{
		super.kill();
		
		FlxG.sound.play(AssetPaths.collectcoin__ogg, 1, false);
	}
}