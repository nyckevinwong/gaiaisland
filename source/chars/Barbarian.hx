package chars;

import chars.Character;
import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import states.Conditions;

class Barbarian extends Character
{
	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		
	
		loadGraphic(AssetPaths.barbarian__png, true, 22, 30);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		facing = FlxObject.RIGHT;
		
		animation.add("standing", [1], 3);
		animation.add("walking", [0, 1,3], 12);
		animation.add("jumping", [2]);
		animation.add("pound", [1]);
		animation.add("landing", [0, 1, 0, 1], 8, false);
		animation.add("grabing-from-down", [4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6],12, false);
		animation.add("attacking", [9,10,11] , 3, false);
	
		
		fsm = new FlxFSM<FlxSprite>(this);
			fsm.transitions
			.add(Idle, Jump, Conditions.jump)
			.add(Idle, Grabbing, Conditions.grab)
			.add(Idle, Attacking, Conditions.fistAttack)
			.add(Attacking, Idle, Conditions.animationFinished)
			.add(Idle, EnterPortalVase, Conditions.enterPortalVase)
			.add(Idle, Climb, Conditions.climb)
			.add(Climb, Idle, Conditions.offLadder)
			.add(Climb, Idle, Conditions.grounded)
			.add(Climb, Jump, Conditions.climbJump)
			.add(Grabbing, Idle, Conditions.animationFinished)
			.add(Jump, Idle, Conditions.grounded)
			.add(Jump, Climb, Conditions.climb)
			.add(Jump, GroundPound, Conditions.groundSlam)
			.add(GroundPound, GroundPoundFinish, Conditions.grounded)
			.add(GroundPoundFinish, Idle, Conditions.animationFinished)
			.start(Idle);
			
	}
	
}

