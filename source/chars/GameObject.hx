package chars;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import maputils.Rect;
import maputils.MapUtils;
import states.Conditions;
import flixel.math.FlxRect;
import flixel.addons.editors.tiled.TiledTileLayer;
import maputils.tile.FlxAnimatedTilemapExt;

/**
 * ...
 * @author ...
 */
class GameObject extends FlxSprite
{
	public static var addSprite: FlxSprite->Void = null;

	public static var map : FlxAnimatedTilemapExt = null;
	public static var tileLayer:TiledTileLayer = null;
	public static var GRAVITY:Float = 600;	
	public static var activeArea:Rect= null;

	
	public var enableMapBoundXCheck: Bool = true;
	public var enableMapBoundYCheck: Bool = true;
	public var isGrabble:Bool = false;
	public var isRiddible:Bool = false;
	
  	private var ACCELERATION_X: Int = 0;
	private var initX: Float;
	private var initY: Float;
	
	private var point:FlxPoint = new FlxPoint();
	
	
	public function new(X:Float = 0, Y:Float = 0)
	{
		super(X, Y);
		initX = X;
		initY = Y;
		
	}
	
	public function resetInitPosition()
	{
		reset(initX, initY);
	}
	
	private function onMapBoundXCheck()
	{
			// if it's out of map boundary on horizontal side	
			if (this.x < 0)
			{
				this.x = 0;
			}
			else if (this.x+this.width > map.width)
			{
				this.x = map.width-this.width;				
			}	
	}
		
	private function onMapBoundYCheck()
	{		
			// if it's out of map boundary on vertical side
			
			if (this.y-this.height < 0) // jump/fly outside the map
			{
				if (this.velocity.y < 0 && this.acceleration.y < 0 )
				{ // the object is flying outside of the screen

				}
			}
			else if (this.y > map.height - this.height*2)
			{
				// fall/jump off from the platfrom/mountain/etc into the death
				this.kill();				
				this.resetInitPosition();
			}
	}
	
	
	public override function update(elapsed:Float):Void 
	{
	
			if (map != null)
			{
				if (this.enableMapBoundXCheck)
				{
					onMapBoundXCheck();
				}
				
				if (this.enableMapBoundYCheck)
				{
					onMapBoundYCheck();
				}
								
			}
		
					
		super.update(elapsed);
	}	
	
	public function getTileCollisionByCoord(tileX:Int, tileY: Int) : Int
	{
				// check the next tile on the right side;	
				if (tileX < 0 || tileX > GameObject.map.widthInTiles-1)
				{
					return FlxObject.NONE;
				}
				
				if (tileY < 0 || tileY > GameObject.map.heightInTiles-1)
				{
					return FlxObject.NONE;
				}
				
				var tileId = GameObject.map.getTile(tileX, tileY);
				var collision:Int = GameObject.map.getTileCollisions(tileId);		
				return collision;
	}
	
	public function isOnLadder(): Bool
	{
		// 16 means tile size
		var quarterWidth: Int = Std.int(width / 2);
		var tileXLeft :Int = Std.int((x + quarterWidth) / 16);   
		var tileXRight :Int = Std.int((x + width-quarterWidth) / 16);   
		var tileY :Int  = Std.int((y + height - 1) / 16);   
	
		var ladderTileID:Int = 141;
		
		// as long as one foot is one the stair
		var onLadder:Bool = getTileByCoord(tileXLeft, tileY) == ladderTileID || getTileByCoord(tileXRight, tileY) == ladderTileID;
		
		return onLadder;
	}
	
	public function overlapWhichTiles(): Array<Int>
	{
		return MapUtils.overlapWhichTiles(x, y, width, height, GameObject.map);
	}
	
	public function areAnyTileOverlapped(targets: Array<Int>) : Bool
	{
		var overlapTiles: Array<Int> = this.overlapWhichTiles();
		
		for (tileId in targets)
		{
			if (overlapTiles.indexOf(tileId) !=-1)
			return true;
		}
		
		return false;
	}
	
	public function onAnyTiles(targets: Array<Int>) : Bool
	{
		var onTiles: Array<Int> = this.onWhichTiles();
		
		for (tileId in targets)
		{
			if (onTiles.indexOf(tileId) !=-1)
			return true;
		}
		
		return false;
	}
	

	public function onWhichTiles(): Array<Int>
	{
		return MapUtils.standOnWhichTiles(GameObject.map, x, y, height);
	}
	
	public function getTileByCoord(x:Int, y: Int) : Int
	{
		return MapUtils.getTileByCoord(GameObject.map, x, y);
	}

	
	
	
	private function updateFacing()
	{
			facing = (acceleration.x > 0) ? FlxObject.RIGHT:FlxObject.LEFT;		
	}
	
	
	/**
	 * change to the opposite direction of the sprite when a sprite hits a collidable tile on its right or left side.
	 * 
	 * @param	X				The X position of the sprite on the map.
	 * @param	Y				The Y position of the sprite on the map.
	 */
	private function changeDirectionWhenHitWall(tileX:Int, tileY:Int)
	{
			if (facing == FlxObject.RIGHT)
			{
				var collision:Int = getTileCollisionByCoord(tileX + 1, tileY);
				
				if((collision & FlxObject.LEFT)==FlxObject.LEFT) // if it hits wall on the right
				{ 
					acceleration.x = -ACCELERATION_X;
					velocity.x = 0;
					updateFacing();
				}
				
			}
			else if (facing == FlxObject.LEFT)
			{
				// check next tile on the left side;				
				var collision:Int = getTileCollisionByCoord(tileX - 1, tileY);
			
				if((collision & FlxObject.RIGHT)==FlxObject.RIGHT) // if it hits wall on the left
				{ 
					acceleration.x = ACCELERATION_X;
					velocity.x = 0;
					updateFacing();
				}
			}
		
	}
	
	/**
	 * change to the opposite direction of the sprite when a sprite hits a non-collidable tile on its lower-right corner or lower-left cornert based on its current direction
	 * 
	 * @param	X				The X position of the sprite on the map.
	 * @param	Y				The Y position of the sprite on the map.
	 */
	private function changeDirectionWhenHitEmptyTileAtBottom(tileX:Int, tileY:Int)
	{
			if (facing == FlxObject.RIGHT)
			{
				var collision:Int = getTileCollisionByCoord(tileX + 1, tileY + 1);
				
				trace("non-collidable:" + collision);

				if(collision==0)// if it is an non-collidalbe tile
				{ 
					acceleration.x = -ACCELERATION_X;
					velocity.x = 0; // velocity must be zero; otherwise, the enemy will still fall by remaning velocity
					updateFacing();
				}
				
			}
			else if (facing == FlxObject.LEFT)
			{
				// check next tile on the left side;				
				var collision:Int = getTileCollisionByCoord(tileX, tileY + 1);
				trace("non-collidable:" + collision);

				if (collision == 0) // if it is an non-collidalbe tile
				{ 
					acceleration.x = ACCELERATION_X;
					velocity.x = 0; // velocity must be zero; otherwise, the enemy will still fall by remaning velocity
					updateFacing();
				}
			}
		
	}
	
	public function checkHeadHitTiles()
	{
		var rect: FlxRect = this.getHitbox(); // check by a hit box or any other box. for example, super mario probably use a fist box.
		
		var tileX: Int = Std.int( this.x/16 );
		var tileY: Int = Std.int( this.y/16) - 1; // must be the tile above the head
		
		var tileXRight: Int = Std.int( (this.x+this.width) /16 );
		
		if (doesHeadHitTileAt(tileX, tileY))
		{
			onHitTilesOnTop(tileX, tileY);
		}
		
		if (tileXRight != tileX && doesHeadHitTileAt(tileXRight, tileY))
		{
			onHitTilesOnTop(tileXRight, tileY);
		}
		
	}
	
	private function doesHeadHitTileAt(tileX:Int, tileY:Int):Bool
	{
		var collision:Int = getTileCollisionByCoord(tileX, tileY);

		if((collision & FlxObject.DOWN)==FlxObject.DOWN )// if it hits on the top
		{ 
			return true;
		}
		
		return false;
	}
	

	private function onHitTilesOnTop(tileX:Int, tileY:Int)
	{

	}
	
	private function onHitTilesAtBottom(tileX:Int, tileY:Int)
	{

	}
	
	private function doesBottomHitTileAt(tileX:Int, tileY:Int):Bool
	{
		var collision:Int = getTileCollisionByCoord(tileX, tileY);

		if((collision & FlxObject.UP)==FlxObject.UP )// if it hits on the top
		{ 
			return true;
		}
		
		return false;
	}
	
	public function checkBottomHitTiles(onCustomHitTilesAtBottom:Int->Int->Void=null)
	{
		var rect: FlxRect = this.getHitbox(); // check by a hit box or any other box. for example, super mario probably use a fist box.
		
		var tileX: Int = Std.int( this.x/16 );
		var tileY: Int = Std.int((this.y+this.height)/16); // must be the tile under the feet
		
		var tileXRight: Int = Std.int( (this.x+this.width) /16 );
		
		if (doesBottomHitTileAt(tileX, tileY))
		{
			if (onCustomHitTilesAtBottom != null)
				onCustomHitTilesAtBottom(tileX, tileY);
			else
				onHitTilesAtBottom(tileX, tileY);
		}
		
		if (tileXRight != tileX && doesBottomHitTileAt(tileXRight, tileY))
		{
			if (onCustomHitTilesAtBottom != null)
				onCustomHitTilesAtBottom(tileXRight, tileY);
			else
				onHitTilesAtBottom(tileXRight, tileY);
		}
		
		
	}


	
}
