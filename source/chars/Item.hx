package chars;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import spriteutils.SpriteAnim;

/**
 * ...
 * @author ...
 */
class Item extends GameObject
{
	public static var updateMaxHealthCallBack: Float->Void = null;
	
	public static inline var SHOTGUN: Int = 1;
	public static inline var APPLE: Int = 3;
	public static inline var PIZZA: Int = 4;
	public static inline var CHESS: Int = 5;
	public static inline var SLIVERKEY: Int = 6;
	public static inline var GOLDKEY: Int = 7;
	public static inline var COIN: Int = 8;
	public var itemID:Int = 0;
	public var startVelocity:FlxPoint;

	private var state:String = "start";
	private var spriteAnim: SpriteAnim;

	private var _spawner: Bool = false;
	public var spawner(get, set) : Bool;
	
	public function new(X:Float = 0, Y:Float = 0, ItemID: Int)
	{
		super(X, Y);
		
		this.itemID = ItemID;
			
		switch(itemID)
		{
			case Item.SHOTGUN:
			loadGraphic(AssetPaths.shotgun_side__png, false);
			case Item.APPLE:
			loadAnimationFrame(Item.APPLE);
			case Item.PIZZA:
			loadAnimationFrame(Item.PIZZA);
			case Item.CHESS:
			loadAnimationFrame(Item.CHESS);
			case Item.SLIVERKEY:
			loadAnimationFrame(Item.SLIVERKEY);			
			case Item.GOLDKEY:
			loadAnimationFrame(Item.GOLDKEY);		
			case Item.COIN:
			// coin from question mark
			loadGraphic(AssetPaths.coinspin__png, true, 16, 16);
			animation.add("idle", [0, 1, 2, 3, 4, 5], 10, true);
			animation.play("idle");
		}

		spriteAnim = new SpriteAnim(this, 0.5, false);		
	}
	
	public function setSpriteAnimDirection( scanDirection: Int )
	{
		this.spriteAnim.scanDirection = scanDirection;
	}
	
	
	private function loadAnimationFrame(frameNum:Int)
	{
		loadGraphic(AssetPaths.objects__png, true, 16, 16);
		animation.add("idle", [frameNum], 1, false);
		animation.play("idle");
	}
	
	public function use(char :Character)
	{
		switch(itemID)
		{
			case Item.SHOTGUN: 
				// you can only take the shotgun if you don't have a weapon in hand
				if (!char.isHolding() && !char.isGrabbing())
				{
					char.holdingTarget = this;
				}
			case Item.CHESS:
						FlxG.sound.play(AssetPaths.increasemaxhealth__ogg, 1, false);						
						Item.updateMaxHealthCallBack(char.maxHealth + 1);
						this.kill();
			case Item.APPLE:
						FlxG.sound.play(AssetPaths.increasehp__ogg, 1, false);						
						char.updateHealth(char.health + 1);
						this.kill();
		}
	}
		
    public function get_spawner():Bool {
        return this._spawner;
    }

    private function set_spawner( v : Bool ):Bool {
		this._spawner = v;
		this.visible = !this._spawner;
		
		
        return this._spawner;
    }
	
	public override function update(elapsed:Float):Void 
	{		
		if (spawner)
		{
			switch(state)
			{
				case "start":
					state = "birth";
				case "birth":
				
				if (spriteAnim.isFinished())
				{
					switch(itemID)
					{
					case Item.COIN:
						this.kill();
						state = "done";
					default:
						if (startVelocity != null)
						{
							ACCELERATION_X = 30;
							this.velocity.x = startVelocity.x;	
							acceleration.x = ACCELERATION_X;						
							this.acceleration.y = GameObject.GRAVITY;
							this.solid = true;
							state = "moving";
						}
					}
				}
				else
				{
					if (!spriteAnim.isStarted())
					{
						switch(itemID)
						{
							case Item.COIN:
							FlxG.sound.play(AssetPaths.hitcoinblock__ogg, 1, false);
							default:
							FlxG.sound.play(AssetPaths.itembirth__ogg, 1, false);
						}
						
						this.visible = true;
					}
					
					spriteAnim.animate(elapsed);
				}
				
				case "moving":
					
					updateFacing();
					
					if (GameObject.map != null)
					{
						
						var x1: Int = Std.int(this.x / 16);
						var y1: Int = Std.int(this.y / 16); 
						
						changeDirectionWhenHitWall(x1, y1);
						
					}
				case "done":
			}
			
		}
		
		super.update(elapsed);	
	}	
	
	private override function onMapBoundYCheck()
	{		
		// if it's out of map boundary on vertical side
		
		if (this.y-this.height < 0) // jump/fly outside the map
		{
			if (this.velocity.y < 0 && this.acceleration.y < 0 )
			{ // the object is flying outside of the screen

			}
		}
		else if (this.y > GameObject.map.height)
		{
			// fall/jump off from the platfrom/mountain/etc into the death
			this.kill();				
			// no need to reset position for Item. item just disappear
//			this.resetInitPosition();
		}
	}


	
}

